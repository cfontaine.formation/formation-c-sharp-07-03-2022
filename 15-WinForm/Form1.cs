﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _08_WinForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Bonjour, "+textBoxPrenom.Text, "Salutation", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void EffacerBtn_Click(object sender, EventArgs e)
        {
            textBoxPrenom.Text = "";
        }

        private void ConfigBtn_Click(object sender, EventArgs e)
        {
            string str=ConfigurationManager.AppSettings["conf1"];
            textBoxPrenom.Text = str;
        }
    }
}

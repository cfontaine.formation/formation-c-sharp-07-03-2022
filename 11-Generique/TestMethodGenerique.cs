﻿using System;

namespace _11_Generique
{
    class TestMethodGenerique
    {
        public static void MethodeGenerique<T>(T a)
        {
            Console.WriteLine(a);
        }

    }
}
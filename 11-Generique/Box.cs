﻿using System;

namespace _11_Generique
{
    class Box<T> //where T : Animal
    {
        public T A { get; set; }
        public T B { get; set; }
        public Box(T a, T b)
        {
            A = a;
            B = b;
        }

        public void Afficher()
        {
            Console.WriteLine($"{A} {B}");
        }

    }

}

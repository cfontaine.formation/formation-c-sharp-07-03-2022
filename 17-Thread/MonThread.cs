﻿using System;
using System.Threading;

namespace _17_Thread
{
    class MonThread
    {
        public int Time { get; private set; }
        public string Name { get; private set; }

        public MonThread(int time, string name)
        {
            Time = time;
            Name = name;
        }

        public void Execute()
        {
            lock (this)
            {
                for (int i = 0; i < 20; i++)
                {
                    Console.WriteLine($"\t{Name}:{i}");
                    Thread.Sleep(Time);
                }
            }
        }
    }

}

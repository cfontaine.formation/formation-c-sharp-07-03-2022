﻿using System;
using System.Diagnostics;
using System.Threading;

namespace _17_Thread
{
    class Program
    {
        static object pblock = new object();

        static void Main(string[] args)
        {
            Console.WriteLine("Début de la méthode Main");

            #region Classe Thread
            // ThreadStart -> Délégué qui permet de créer Thread (Méthode sans paramètre)
            Thread tsp = new Thread(new ThreadStart(ThreadSansParam)); // Le constructeur de la classe Thread prend en paramètre un délégué
            tsp.Start(); //  La Méthode Start permet de démmarrer le Thread

            // ParamterizedThreadStart -> Délégué qui permet de créer Thread (méthode qui a un paramètre de "type" object)
            Thread tap = new Thread(new ParameterizedThreadStart(ThreadMethode));
            tap.Start("Thread avec paramètre");
            #endregion

            #region Propriété
            Thread t1 = new Thread(new ParameterizedThreadStart(ThreadMethode));
            t1.Name = "T1";    // La propriété Name permet de nommer le Thread
            t1.Priority = ThreadPriority.Lowest; // La propriété Priority permet de modifier la prioritédu thread
                                                 // Highest => le thread est le + prioritaire, AboveNormal, Normal (par défaut), BelowNormal, Lowest => le thread est le - prioritaire
            Console.WriteLine($"état du Thread à la création = {t1.ThreadState}");    // La propriétée ThreadState contient l'état du Thread, au départ => Unstarted
            Thread t2 = new Thread(new ParameterizedThreadStart(ThreadMethode));
            t2.Name = "T2";
            t2.Priority = ThreadPriority.Highest;
            // 
            t1.Start(100);
            t2.Start(200);
            Console.WriteLine($"état du Thread après L'éxécution de la méthode Start= {t1.ThreadState}"); // Runnable ou running s'il est executé
            #endregion

            #region Méthode Arbort
            // Un thread démarre avec l'execution de la méthode Start et finit lorsque la méthode associé est finie 
            // On peut arréter les Thread aussi avec la méthode Arbort
            try
            {
                t1.Abort();
                Console.WriteLine($"état du Thread après l'arrét du Thread avec La méthode Arbort= {t1.ThreadState}");
                t2.Abort();
            }
            catch (ThreadAbortException e)
            {
                Console.WriteLine($"Problème Abort {e.StackTrace}");
            }
            #endregion

            #region Thread join
            // Join 
            // bloque tous les threads appelants jusqu'à ce que ce thread se termine
            MonThread mt1 = new MonThread(50, "MT1");
            MonThread mt2 = new MonThread(100, "MT2");
            Thread t3 = new Thread(new ThreadStart(mt1.Execute));
            Console.WriteLine(t1.ThreadState);
            Thread t4 = new Thread(new ThreadStart(mt2.Execute));
            t3.Start();
            t3.Join();  // Attend que le thread t3 soit finit pour démarrer t4
            t4.Start();
            #endregion

            #region Verrouiller un Thread
            Thread t5 = new Thread(new ThreadStart(ThreadLockTest));
            Thread t6 = new Thread(new ThreadStart(ThreadLockTest));
            t5.Start();
            t5.Name = "t5";
            t6.Start();
            t6.Name = "t6";
            #endregion

            #region Timer
            Timer t = new Timer(xyz, "Message du timer",
            0,          //appel tout de suite
            1000);      //et toutes les secondes
            #endregion

            #region Processus
            Process p2 = Process.Start("Notepad.exe");
            #endregion
            Console.WriteLine("Fin de la méthode Main");
            Console.ReadKey();
        }

        public static void ThreadSansParam()
        {
            for (int i = 0; i < 30; i++)
            {
                Console.WriteLine($"Thread sans paramètre i={i}");
            }
        }

        public static void ThreadMethode(object time)
        {
            Thread current = Thread.CurrentThread;  // propriété statique qui permet d'accéder au thread en cours
            Console.WriteLine($"Début du Thread {current.Name} {current.Priority} {time}ms ");
            for (int i = 0; i < 30; i++)
            {
                Console.WriteLine($"Thread {current.Name} i={i}");
                if (time is int t)
                {
                    Thread.Sleep(t);   // Pour suspendre le thread actuel pendant les t millisecondes
                }
            }
            Console.WriteLine($"Fin du Thread {current.Name}");

        }

        public static void ThreadLockTest()
        {
            Thread current = Thread.CurrentThread;
            Console.WriteLine($"Début du Thread {current.Name}");
            lock (pblock)   // entoure la potion de code qui doit être verrouillée un seul à la fois ne peut y accéder
            {
                Console.WriteLine($"Début  Lock {current.Name}");
                for (int i = 0; i < 4; i++)
                {
                    Console.WriteLine($"Lock {i}");
                }
                Console.WriteLine($"Fin Lock {current.Name}");
            }
        }
        static void xyz(object state)
        {
            Console.WriteLine(state);
        }

    }
}

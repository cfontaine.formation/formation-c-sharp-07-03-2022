﻿using System;

namespace _03_Tableaux
{
    class Program
    {
        static void Main(string[] args)
        {
            #region tableau à une dimension

            // Déclarer un tableau

            //double[] tab;
            //tab = new double[5];

            double[] tab = new double[5];

            // Valeur d'nitialisation des éléments du tableau
            // - entier -> 0
            // - double , float ou un decimal -> 0.0
            // - char -> '\u0000'
            // - boolean -> false
            //  -type référence  -> null

            // Accèder à un élément du tableau
            Console.WriteLine(tab[1]);
            tab[0] = 4.5;
            // si l'on essaye d'accèder à un élément en dehors du tableau => Exception
            // Console.WriteLine(tab[9]);   

            // Nombre d'élément du tableau
            Console.WriteLine(tab.Length);

            // Parcourir complétement un tableau (avec un for) 
            for (int i = 0; i < tab.Length; i++)
            {
                Console.WriteLine($"tab[{i}]={tab[i]}");
            }

            // Parcourir complétement un tableau (foreach)
            // foreach (double elm in tab) // ou
            foreach (var elm in tab)    //  var -> elm est de type double
            {
                Console.Write($"{elm} "); ; // elm uniquement en lecture
                                        //elm = 7.8; // => Erreur ,on ne peut pas modifier elm
            }
            Console.WriteLine("");

            //string[] tabStr = new string[3]; // Tous les éléments sont initialisés à null
            //tabStr[0]="bonjour";
            //tabStr[1] = "bonjour";
            //tabStr[2] = "bonjour";

            // Déclaration et initialisation
            string[] tabStr2 = { "bonjour", "hello", "world" };
            Console.WriteLine(tabStr2.Length);
            foreach (var s in tabStr2)
            {
                Console.WriteLine(s);
            }

            // Saisir la taille du tableau
            Console.Write("Saisir le nombre d'élément du tableau: ");
            int tailleT = Convert.ToInt32(Console.ReadLine());
            int[] t = new int[tailleT];
            foreach (var v in t)
            {
                Console.WriteLine(v);
            }
            Console.WriteLine($"Nombre d'élément= {t.Length}");
            #endregion

            #region Exercice Tableau
            // 1 - Trouver la valeur maximale et la moyenne d’un tableau de 5 entier: -7, -6, -4, -8, -3
            // 2 - Modifier le programme pour faire la saisie de la taille du tableau et saisir les éléments du tableau

            // int[] tabInt = { -7, -6, -4, -8, -3 }; // 1

            Console.Write("Entrer la taille du tableau "); //2
            int size = Convert.ToInt32(Console.ReadLine());

            int[] tabInt = new int[size];
            for (int i = 0; i < size; i++)
            {
                Console.Write($"tabInt[{i}]= ");
                tabInt[i] = Convert.ToInt32(Console.ReadLine());
            }

            int max = tabInt[0]; // int.Max;
            double somme = 0.0;
            foreach (var v in tabInt)
            {
                if (v > max)
                {
                    max = v;
                }
                somme += v;
            }
            Console.WriteLine($"Maximum={max} Moyenne={somme / tabInt.Length}");
            #endregion

            #region Tableau Multidimmension
            int[,] tab2d = new int[4, 3];   // Déclaration d'un tableau à 2 dimensions

            tab2d[0, 0] = 123;  // Accès à un élémént d'un tableau à 2 dimensions
            Console.WriteLine(tab2d[1, 2]); // 0

            // Nombre d'élément du tableau
            Console.WriteLine(tab2d.Length); // 12

            // Nombre de dimension du tableau 
            Console.WriteLine(tab2d.Rank); // 2

            // Nombre de ligne du tableau
            Console.WriteLine(tab2d.GetLength(0)); //  4

            // Nombre de colonne du tableau
            Console.WriteLine(tab2d.GetLength(1)); // 3

            // Parcourir complétement un tableau à 2 dimension => for
            for (int i = 0; i < tab2d.GetLength(0); i++)
            {
                for (int j = 0; j < tab2d.GetLength(1); j++)
                {
                    Console.Write($"{tab2d[i, j]}\t");
                }
                Console.WriteLine("");
            }

            // Parcourir complétement un tableau à 2 dimension => foreach
            foreach (var elm in tab2d) // avec var la variable elm aura le même type que le tableau 
            {
                Console.Write($"{elm} ");
            }
            // Ou
            //foreach (int elm in tab2d) 
            //{
            //    Console.WriteLine(elm);
            //}
            Console.WriteLine("");

            // Déclarer un tableau à 2 dimension en l'initialisant
            char[,] tabChr = { { 'a', 'z', 'e' }, { 'r', 't', 'y' } };
            foreach (var elm in tabChr)
            {
                Console.Write($"{elm} ");
            }
            Console.WriteLine("");
            // Tableau 3D
            // Déclaration d'un tableau à 3 dimensions
            double[,,] tab3D = new double[2, 3, 4];

            // Accès à un élément d'un tableau à 3 dimensions
            tab3D[1, 0, 2] = 4.67;

            // Nombre d'élément => 24 Nombre de dimension du tableau => 3
            Console.WriteLine($"{tab3D.Length} {tab3D.Rank}");

            // Afficher le nombre d'élément de chaque dimmension
            for (int i = 0; i < tab3D.Rank; i++)
            {
                Console.WriteLine($"Dimmension {i} ={tab3D.GetLength(i)}");
            }
            #endregion

            #region tableau de tableau
            // Déclaration d'un tableau de tableau
            int[][] tabEsc = new int[4][];
            tabEsc[0] = new int[2];
            tabEsc[1] = new int[4];
            tabEsc[2] = new int[3];
            tabEsc[3] = new int[2];

            // Accès à un élément
            tabEsc[1][2] = 45;

            // Nombre de ligne
            Console.WriteLine(tabEsc.Length); // 4
            // Nombre de colonne de la première ligne
            Console.WriteLine(tabEsc[0].Length); //2
            // Nombre de colonne de la 2ème ligne
            Console.WriteLine(tabEsc[1].Length); //4

            //  Parcourir complétement un tableau de tableau => for
            for (int i = 0; i < tabEsc.Length; i++)
            {
                for (int j = 0; j < tabEsc[i].Length; j++)
                {
                    Console.Write($"{tabEsc[i][j]}\t");
                }
                Console.WriteLine("");
            }

            //  Parcourir complétement un tableau de tableau => foreach
            foreach (var row in tabEsc) // row type -> int []
            {
                foreach (var elm in row) // elm type -> int
                {
                    Console.Write($"{elm}\t");
                }
                Console.WriteLine("");
            }
            // Déclarer et initialiser un tableau de tableau
            int[][] tabEsc2 = new int[][] { new int[] { 2, 5, 3 }, new int[] { 1, 2, 3, 6, 8 } };

            foreach (var row in tabEsc2)
            {
                foreach (var elm in row)
                {
                    Console.Write($"{elm} ");
                }
            }
            Console.WriteLine("");
            #endregion
            Console.ReadKey();
        }
    }
}


﻿using _14C_BibliothequeBddGeneric;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;

namespace _16_Annuaire
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ContactDao.ChaineConnection = ConfigurationManager.ConnectionStrings["chCnxSqlSever"].ConnectionString;
            InitListView(new ContactDao().FindAll());
        }

        private void ajouterBtn_Click(object sender, EventArgs e)
        {
            ContactDao dao = new ContactDao();
            SaisirContact(dao);
            InitListView(dao.FindAll());
        }

        private void modifierBtn_Click(object sender, EventArgs e)
        {
            ContactDao dao = new ContactDao();
            if (SelectContact(dao, out Contact c))
            {
                SaisirContact(dao, c);
                InitListView(dao.FindAll());
            }
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            ContactDao dao = new ContactDao();
            if (SelectContact(dao, out Contact c))
            {
                dao.Delete(c, false);
                InitListView(dao.FindAll());
            }
        }

        private void quitBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        private void InitListView(List<Contact> contacts)
        {
            listView1.Items.Clear();
            foreach (Contact c in contacts)
            {
                AddListView(c);
            }
        }

        private void AddListView(Contact contact)
        {
            ListViewItem item1 = new ListViewItem(contact.Nom, 0);
            item1.SubItems.Add(contact.Prenom);
            item1.SubItems.Add(contact.DateNaissance.ToShortDateString());
            item1.SubItems.Add(contact.Email);
            listView1.Items.Add(item1);
        }


        private bool SelectContact(ContactDao dao, out Contact contact)
        {
            var selected = listView1.SelectedItems;
            if (selected.Count == 0)
            {
                MessageBox.Show("Il n'a pas de ligne sélectionner", "Modifier un contact", MessageBoxButtons.OK, MessageBoxIcon.Error);
                contact = null;
            }
            else
            {
                contact = dao.FindByEmail(selected[0].SubItems[3].Text, false);
            }
            return contact != null;
        }

        private void SaisirContact(ContactDao dao, Contact c = null)
        {
            Form2 f = new Form2(c);
            if (f.ShowDialog(this) == DialogResult.OK)
            {

                if (dao.IsEmailExist(f.ContactForm.Email, false) && (f.ContactForm.Id == 0 || (c != null && f.ContactForm.Id == c.Id)))
                {
                    MessageBox.Show(this, "L'email existe déjà", "Erreur unicité email", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    dao.SaveOrUpdate(f.ContactForm);
                }
            }
            f.Close();
            f.Dispose();
        }
    }
}

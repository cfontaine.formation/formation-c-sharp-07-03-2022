﻿using System;

namespace _02_Instructions
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Condition
            // Condition if
            Console.Write("Saisir un entier: ");
            int cn = Convert.ToInt32(Console.ReadLine());
            if (cn > 10)
            {
                Console.WriteLine("La valeur est supérieur à 10");
            }
            else if (cn == 10)
            {
                Console.WriteLine("La valeur est égale à 10");
            }
            else
            {
                Console.WriteLine("La valeur est inférieur à 10");
            }

            // Exercice: Trie de 2 Valeurs
            // Saisir 2 nombres et afficher ces nombres triés dans l'ordre croissant sous la forme 1.5 < 10.5
            Console.Write("Saisir un entier: ");
            int va = Convert.ToInt32(Console.ReadLine());
            Console.Write("Saisir un entier: ");
            int vb = Convert.ToInt32(Console.ReadLine());
            if (va < vb)
            {
                Console.WriteLine($" {va}<{vb}");
            }
            else if (va == vb)
            {
                Console.WriteLine($" {vb}={va}");
            }
            else
            {
                Console.WriteLine($" {vb}<{va}");
            }

            // Exercice: Intervalle
            // Saisir un nombre et dire s'il fait parti de l'intervalle - 4(exclu) et 7(inclu)
            Console.Write("Saisir un entier: ");
            int vi = Convert.ToInt32(Console.ReadLine());
            if (vi > -4 && vi <= 7)
            {
                Console.WriteLine($"{vi} fait partie de l'intervalee -4 7");
            }

            // Condition switch
            Console.Write("Saisir un jour (1 à 7): ");
            int jours = Convert.ToInt32(Console.ReadLine());
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    break;
                case 6:
                case 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            // Exercice: Calculatrice
            // Faire un programme calculatrice
            // Saisir dans la console
            // - un nombre à virgule flottante v1
            // - une chaîne de caractère opérateur qui a pour valeur valide:  + - * /
            // - un nombre à virgule flottante v2
            // Afficher:
            // - Le résultat de l’opération
            // - Un message d’erreur si l’opérateur est incorrect
            // - Un message d’erreur si l’on fait une division par 0
            Console.WriteLine("Calculatrice");
            double v1 = Convert.ToDouble(Console.ReadLine());
            string op = Console.ReadLine();
            double v2 = Convert.ToDouble(Console.ReadLine());

            switch (op)
            {
                case "+":
                    Console.WriteLine($"{v1} + {v2} = {v1 + v2}");
                    break;
                case "-":
                    Console.WriteLine($"{v1} - {v2} = {v1 - v2}");
                    break;
                case "*":
                    Console.WriteLine($"{v1} * {v2} = {v1 * v2}");
                    break;
                case "/":
                    if (v2 == 0.0) // (v2> -0.000001 && v2<0.000001)
                    {
                        Console.WriteLine("Division par 0");
                    }
                    else
                    {
                        Console.WriteLine($"{v1} / {v2} = {v1 / v2}");
                    }
                    break;
                default:
                    Console.WriteLine($"L'opérateur {op} n'est pas valide");
                    break;
            }

            // Clause when (C# 7.0) => Ajouter une condition supplémentaire sur un case
            Console.Write("Saisir un jour (1 à 7): ");
            jours = Convert.ToInt32(Console.ReadLine());
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    break;
                case int j when j >= 6 && j <= 7:   // <== clause when
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            // Opérateur ternaire
            Console.Write("Saisir une valeur double: ");
            double va1 = Convert.ToDouble(Console.ReadLine());
            double abs = va1 >= 0.0 ? va1 : -va1;
            Console.WriteLine($"abs={abs}");
            #endregion

            #region boucle
            // Boucle while 
            int inc = 0;
            while (inc < 5)
            {
                Console.WriteLine(inc);
                inc++;
            }

            // Boucle do while
            inc = 0;
            do
            {
                Console.WriteLine(inc);
                inc++;
            }
            while (inc < 5);

            // Boucle for
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }

            //for(; ; ) // Boucle infinie
            //{

            //}
            // ou
            //while (true)
            //{

            //}

            // Instruction de saut
            // break
            for (int i = 0; i < 10; i++)
            {
                if (i == 3)
                {
                    break;
                }
                Console.WriteLine(i);
            }

            // continue
            for (int i = 0; i < 10; i++)
            {
                if (i == 3)
                {
                    continue;
                }
                Console.WriteLine(i);
            }

            // Série harmonique
            // On saisit un nombre entier __n__ et on calcule la somme des n premiers termes de la série harmonique:
            // 1 + 1 / 2 + 1 / 3 + 1 / 4 + ... +1 / n
            Console.Write("n= ");
            int n = Convert.ToInt32(Console.ReadLine());
            double harm = 1.0;
            for (int i = 2; i <= n; i++)
            {
                harm += 1.0 / i;
            }
            Console.WriteLine($"Série harmonique={harm}");

            // Quadrillage 
            // Créer un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne
            //    ex: pour 2 3
            //    [ ][ ]
            //    [ ][ ]
            //    [ ][ ]
            Console.Write("Nombre de colonne=");
            int co = Convert.ToInt32(Console.ReadLine());
            Console.Write("Nombre de ligne=");
            int li = Convert.ToInt32(Console.ReadLine());

            for (int l = 0; l < li; l++)
            {
                for (int c = 0; c < co; c++)
                {
                    Console.Write("[ ]");
                }
                Console.WriteLine("");
            }

            // goto avec un switch
            Console.Write("Saisir un jour (1 à 7): ");
            jours = Convert.ToInt32(Console.ReadLine());
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    //  goto case 6;
                    goto default;
                case 6:
                case 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            // goto pour quitter des boucles imbriquées
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Console.WriteLine($" {i} {j}");
                    if (i == 3)
                    {
                        goto EXIT_LOOP;
                    }
                }
            }

        EXIT_LOOP: Console.ReadKey();
            #endregion
        }
    }
}

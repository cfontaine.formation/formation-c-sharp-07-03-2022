﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _10_ClasseDotNet
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Chaine de caractère
            string str = "Hello";
            Console.WriteLine(str);
            string str2 = new string('a', 10);
            Console.WriteLine(str2);

            // Length -> Nombre de caractère de la chaine de caractère
            Console.WriteLine(str2.Length); // 10

            // On peut accèder à un caractère de la chaine comme à un élément d'un tableau (index commence à 0)
            Console.WriteLine(str[1]); // e

            // Concaténation 
            // avec l'opérateur + ou avec la méthode de classe Concat
            Console.WriteLine(str + " World");
            string str3 = string.Concat(str, " World");
            Console.WriteLine(str3);
            Console.WriteLine(str);

            // La méthode Join concatène les chaines, en les séparants par une chaine de séparation
            string str4 = string.Join(";", "azerty", "iop", "sdfghj", "wxcv");
            Console.WriteLine(str4);

            // Découpe la chaine en un tableau de sous-chaine suivant un ou plusieurs séparateur, par défaut le caractère espace
            string[] elms = str4.Split(';');
            foreach (string e in elms)
            {
                Console.WriteLine(e);
            }

            // Substring permet d'extraire une sous-chaine d'une chaine
            // de l'indice passé en paramètre juqu'à la fin de la chaine
            Console.WriteLine(str3.Substring(6)); // World
            // ou pour le nombre de caractères passé en paramètre
            Console.WriteLine(str3.Substring(6, 2)); //Wo

            // Insère la chaine à partir de l'indice passé en paramètre
            Console.WriteLine(str3.Insert(5, "-----------------------"));
            Console.WriteLine(str3.Remove(5, 4));

            // StartsWith retourne true si la chaine commence par la chaine passé en paramètre
            Console.WriteLine(str3.StartsWith("Hell")); //true
            Console.WriteLine(str3.StartsWith("aaaa")); //false

            // IndexOf retourne la première occurence du caractère ou de la chaine passée en paramètre
            Console.WriteLine(str3.IndexOf('o')); // 4
            Console.WriteLine(str3.IndexOf('o', 5)); // 7 idem mais à partir de l'indice passé en paramètre
            Console.WriteLine(str3.IndexOf('o', 8)); //-1// retourne -1, si le caractère n'est pas trouvé

            // Remplace toutes les chaines (ou caratères) oldValue par newValue 
            Console.WriteLine(str3.Replace('o', 'a'));

            // Retourne True si la sous-chaine passée en paramètre est contenu dans la chaine
            Console.WriteLine(str3.Contains("lo")); // true
            Console.WriteLine(str3.Contains("aaaa")); //false

            // Aligne les caractères à droite en les complétant par un caractère à gauche pour une longueur spécifiée
            Console.WriteLine(str3.PadLeft(40, '_'));

            // Aligne les caractères à gauche en les complétant par un caractère à droite pour une longueur spécifiée
            Console.WriteLine(str3.PadRight(40, '_'));

            // ToUpper convertie tous les caractères en majuscule
            Console.WriteLine(str3.ToUpper());

            // ToLower convertie tous les caractères en minuscule
            Console.WriteLine(str3.ToLower());

            // Trim supprime les caractères de blanc du début et de la fin de la chaine
            Console.WriteLine("   \n \t    Hello \n \nBonjour     \n \t \b".Trim());
            Console.WriteLine("   \n \t    Hello \n \nBonjour     \n \t \b".TrimStart());   // idem uniquement en début de chaine
            Console.WriteLine("   \n \t    Hello \n \nBonjour     \n \t \b".TrimEnd());     // idem uniquement en fin de chaine

            // Comparaison 0-> égale, 1-> se trouve après, -1 -> se trouve avant dans l'ordre alphabétique
            // Il existe 2 méthodes: une d'instance et de classe
            Console.WriteLine(str.CompareTo("Hello"));
            Console.WriteLine(str.CompareTo("World"));
            Console.WriteLine(string.Compare("World", str));

            // On peut tester l'égalité de 2 chaines de caractères avec Equals et l'opérateur ==
            Console.WriteLine("Hello".Equals(str));
            Console.WriteLine(str == "Hello");

            // On peut chainer l'appel des différentes méthodes
            Console.WriteLine(str3.ToUpper().Substring(6).PadRight(13, '_'));

            // Lorsqu'il y a beaucoup de manipulation (+de 3) sur une chaine ( concaténation et insertion, remplacement,supression de sous-chaine)
            // il vaut mieux utiliser un objet StringBuilder qui est plus performant qu'un objet String => pas de création d'objet intermédiare
            StringBuilder sb = new StringBuilder("Test");
            sb.Append("___");
            sb.Append(12.3);
            sb.Remove(4, 2);
            string str6 = sb.ToString();    // Convertion d'un StringBuilder en une chaine de caractères 
            Console.WriteLine(str6);
            #endregion

            #region Date
            DateTime d = DateTime.Now;  // DateTime.Now => récupérer l'heure et la date courante
            Console.WriteLine(d);
            Console.WriteLine(d.Year);
            Console.WriteLine(d.DayOfWeek);
            Console.WriteLine(d.DayOfYear);
            Console.WriteLine(d.Hour);
            Console.WriteLine(d.Minute);

            DateTime dUtc = DateTime.UtcNow;
            Console.WriteLine(dUtc);

            DateTime noel2021 = new DateTime(2021, 12, 25);
            Console.WriteLine(noel2021);
            Console.WriteLine(noel2021 - d);

            // TimeSpan => représente une durée
            TimeSpan dixJours = new TimeSpan(10, 0, 0, 0);
            Console.WriteLine(d.Add(dixJours));
            Console.WriteLine(d.AddMonths(3));

            Console.WriteLine(d.CompareTo(noel2021)); // -1
            Console.WriteLine(noel2021.CompareTo(d)); //  1

            Console.WriteLine(d.ToLongDateString());
            Console.WriteLine(d.ToShortDateString());
            Console.WriteLine(d.ToLongTimeString());
            Console.WriteLine(d.ToShortTimeString());

            Console.WriteLine(d.ToString("dd MMM yy"));
            DateTime d2 = DateTime.Parse("2003/03/12");
            Console.WriteLine(d2);
            #endregion
            #region Collection
            // Collection faiblement typé => elle peut contenir tous types d'objets
            ArrayList lst = new ArrayList();
            lst.Add("hello");
            lst.Add("World");
            lst.Add("Bonjour");
            lst.Add(3.5);
            lst.Add(1.0);
            if (lst[0] is string strC)
            {
                Console.WriteLine(strC);
            }

            // Collection fortement typée => type générique
            List<string> lstStr = new List<string>();
            lstStr.Add("hello");
            lstStr.Add("World");
            lstStr.Add("Bonjour");
            //lstStr.Add(3.5);      // On ne peut plus qu'ajouter des chaines de caractères => sinon erreur de complilation

            string resStr = lstStr[0];  // Accès à un élément de la liste
            lstStr[0] = "azerty";

            foreach (var s in lstStr)   // Parcourir la collection complétement
            {
                Console.WriteLine(s);
            }

            Console.WriteLine(lstStr.Count);    // Nombre d'élément de la collection
            Console.WriteLine(lstStr.Max());    // Valeur maximum stocké dans la liste
            lstStr.Reverse();                   // Inverser l'ordre de la liste
            foreach (var s in lstStr)
            {
                Console.WriteLine(s);
            }

            // Parcourrir un collection avec un Enumérateur
            IEnumerator<string> it = lstStr.GetEnumerator();
            it.Reset();
            while (it.MoveNext())
            {
                Console.WriteLine(it.Current);
            }

            // Dictionary => association clé/valeu
            Dictionary<int, string> m = new Dictionary<int, string>();
            m.Add(123, "hello");        // Add => ajout d'un valeur associé à une clé
            // m.Add(123, "erreur");    // On ne peut pas ajouter, si la clé éxiste déjà => exceptio
            m.Add(12, "Bonjour");
            m.Add(4, "A supprimer");

            // accès à un élément m[clé] => valeur
            Console.WriteLine(m[12]);
            m[12] = "World";
            m.Remove(4);

            // Parcourir un dictionnary
            foreach (var kv in m)
            {
                Console.WriteLine($"clé={kv.Key} Valeur={kv.Value}");
            }
            #endregion
            Console.ReadKey();

        }
    }
}

﻿using System;

namespace _04_Methodes
{
    class Program
    {
        static void Main(string[] args)
        {
            // Appel de methode
            double res = Multiplier(2, 3);
            Console.WriteLine(res);
            Console.WriteLine(Multiplier(5, 6));

            // Appel de methode (sans type retour)
            Afficher(3);

            // Exercice Maximum
            Console.Write("Saisir un entier: ");
            int v1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Saisir un entier: ");
            int v2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(Maximum(v1, v2));

            // Exercice Paire
            Console.WriteLine(Even(4));
            Console.WriteLine(Even(5));

            // Passage par valeur (par défaut en C#)
            // C'est une copie de la valeur du paramètre qui est transmise à la méthode
            int v = 42;
            TestParamValeur(v);
            Console.WriteLine(v); //42

            // Passage de paramètre par référence
            // La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
            TestParamReference(ref v);
            Console.WriteLine(v); //1000

            // Passage de paramètre en sortie
            // Un paramètre out ne sert à la méthode qu'à retourner une valeur
            // L'argument transmis doit référencer une variable ou un élément de tableau
            // La variable n'a pas besoin d'être initialisée

            // int a,  b;
            // On peut déclarer la variable de retour dans les arguments pendant l'appel de la méthode
            TestParamSortie(out int a, out int b);
            Console.WriteLine(a + " " + b);

            // On peut ignorer un paramètre out en le nommant _
            TestParamSortie(out _, out int c);
            Console.WriteLine(c);

            // Paramètre optionnel
            TestParamOptionnel(false, 12.6, 10);
            TestParamOptionnel(true, 3.14);
            TestParamOptionnel(false);

            // Paramètres nommés
            TestParamOptionnel(true, i: 34);
            TestParamOptionnel(i: 34, b: false);

            // Nombre de paramètres variable
            Console.WriteLine(Moyenne(2));
            Console.WriteLine(Moyenne(2, 1));
            Console.WriteLine(Moyenne(2, 1, 5, 10, 45));

            int[] t1 = { 3, 7, 10, 6, 34 };
            Console.WriteLine(Moyenne(4, t1));

            // passage de paramètre référence en entrée => in
            byte[] pix = new byte[1000];
            Image img = new Image(pix);
            TestParamIn(img);

            // Passage d'un tableau en paramètre
            double[] td = { 2.5, 4.6, 7.5, 1.4 };
            TestParamTableau(td);

            // Exercice Echange
            int x = 10;
            int y = 20;
            Console.WriteLine($"x={x} y={y}");
            Swap(ref x, ref y);
            Console.WriteLine($"x={x} y={y}");

            // Exercice Tableau
            int[] t = SaisirTab();
            AfficheTab(t);
            CaculTab(t, out int maximum, out double moyenne);
            Console.WriteLine($"Maximum={maximum} Moyenne={moyenne}");

            // Surcharge de méthode

            // Correspondance exacte des type des paramètres
            Console.WriteLine(Somme(1, 4));
            Console.WriteLine(Somme(2.5, 4.6));
            Console.WriteLine(Somme(2, 8.6));
            Console.WriteLine(Somme("aze", "rty"));

            // Pas de correspondance exacte => convertion automatique
            Console.WriteLine(Somme(3.45, 1));  // => appel de la méthode avec 2 double en paramètres
            Console.WriteLine(Somme(12L, 1L));  // => appel de la méthode avec 2 double en paramètres
            Console.WriteLine(Somme('a', 'e')); // => appel de la méthode avec 2 entiers en paramètres

            // Pas de conversion possible => Erreur
            // Console.WriteLine(Somme(3.4, 2.3M)) ;

            // Méthode récursive
            int fa = Factorial(3);
            Console.WriteLine(fa);

            // Affichage des paramètres passés au programme (main)
            // En lignes de commandes: 05-methode.exe Azerty 12234 "aze rty"
            // Dans visual studio: propriétés du projet-> onglet Déboguer-> options de démarrage -> Arguments de la ligne de commande 
            foreach (var arg in args)
            {
                Console.WriteLine(arg);
            }

            Console.ReadKey();
        }

        // Déclaration
        static double Multiplier(double d1, double d2)
        {
            return d1 * d2; // L'instruction return
                            // - Interrompt l'exécution de la méthode
                            // - Retourne la valeur de l'expression à droite
        }

        static void Afficher(int v) // void => pas de valeur retourné
        {
            int i = 3;
            Console.WriteLine($"{v} {i}");
            // avec void => return; ou return peut être omis ;
        }

        #region Exercice Maximum
        // Ecrire une méthode Maximum qui prends en paramètre 2 nombres et elle retourne le maximum
        static int Maximum(int a, int b)
        {
            if (a < b)
            {
                return b;
            }
            else
            {
                return a;
            }
            // ou
            //    return a < b ? b : a;
        }
        #endregion

        #region Exercice Paire
        // Écrire une méthode Paire qui prend un entier en paramètre un entier
        // Elle retourne vrai, si il est paire
        static bool Even(int v)
        {
            if (v % 2 == 0) // ou v & 1 == 0
            {
                return true;
            }
            else
            {
                return false;
            }
            // ou
            //   return v % 2 == 0;
        }
        #endregion

        #region Passage de paramètre

        // Passage par valeur
        static void TestParamValeur(int a)
        {
            Console.WriteLine(a);
            a = 1000;   // La modification de la valeur du paramètre x n'a pas d'influence en dehors de la méthode
            Console.WriteLine(a);
        }

        // Passage par référence => ref
        static void TestParamReference(ref int a)
        {
            Console.WriteLine(a);
            a = 1000;
            Console.WriteLine(a);
        }

        // Passage de paramètre en sortie => out
        static void TestParamSortie(out int x, out int y)
        {
            // int t = x * 2;   // erreur 
            x = 100;    // La méthode doit obligatoirement affecter une valeur aux paramètres out
            y = x * 4;
        }

        // On peut donner une valeur par défaut aux arguments passés par valeur
        static void TestParamOptionnel(bool b, double d = 4.5, int i = 2)
        {
            Console.WriteLine($"{b} {d} {i}");
        }

        // Nombre d'arguments variable => params
        static double Moyenne(int v1, params int[] valeur)
        {
            double somme = v1;
            foreach (var v in valeur)
            {
                somme += v;
            }
            return somme / (valeur.Length + 1);
        }

        // Passage de paramètre parréférence en entré => in
        // Identique à un passage par valeur mais, il n'y a pas de copie
        // On ne peut pas modifier le paramètre dans la méthode sinon => erreur
        static void TestParamIn(in Image im)
        {
            Console.WriteLine(im.getPixels().Length);
        }

        // Passage en paramètre d'un tableau
        static void TestParamTableau(double[] tab)
        {
            Console.WriteLine(tab.Length);
        }
        #endregion

        #region Exercice Echange
        // Écrire une méthode Swap qui prend en paramètre 2 entiers et qui permet d'inverser le contenu des 2 variables passées en paramètre
        static void Swap(ref int a, ref int b)
        {
            int tmp = a;
            a = b;
            b = tmp;
        }
        #endregion

        #region Exercice Tableau
        // Écrire un méthode qui affiche un tableau d’entier
        // Écrire une méthode qui calcule :
        // - le maximum d’un tableau d’entier
        // - la moyenne

        static void AfficheTab(int[] tab)
        {
            Console.Write("[ ");
            foreach (var elm in tab)
            {
                Console.Write($"{elm} ");
            }
            Console.WriteLine("]");
        }

        static int[] SaisirTab()
        {
            Console.Write("Saisir le nombre d'élément du tableau: ");
            int size = Convert.ToInt32(Console.ReadLine());
            int[] tabInt = new int[size];
            for (int i = 0; i < size; i++)
            {
                Console.Write($"tabInt[{i}]= ");
                tabInt[i] = Convert.ToInt32(Console.ReadLine());
            }
            return tabInt;
        }

        static void CaculTab(int[] tabInt, out int maximum, out double moyenne)
        {
            maximum = tabInt[0];
            double somme = 0.0;
            foreach (var v in tabInt)
            {
                if (v > maximum)
                {
                    maximum = v;
                }
                somme += v;
            }
            moyenne = somme / tabInt.Length;
        }
        #endregion

        #region Surcharge de méthode
        // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même nom, mais leurs signatures doient être différentes
        // La signature d'une méthode correspond aux types et nombre de paramètres
        // Le type de retour ne fait pas partie de la signature
        static int Somme(int a, int b)
        {
            Console.WriteLine("2 entiers");
            return a + b;
        }

        static double Somme(double d1, double d2)
        {
            Console.WriteLine("2 doubles");
            return d1 + d2;
        }

        static double Somme(int d1, double d2)
        {
            Console.WriteLine("1 entier 1 double");
            return d1 + d2;
        }

        static string Somme(string a, string b)
        {
            Console.WriteLine("2 chaine");
            return a + b;
        }
        #endregion

        #region Méthode récursive
        static int Factorial(int n) // factoriel= 1* 2* … n
        {
            if (n <= 1) // condition de sortie
            {
                return 1;
            }
            else
            {
                return Factorial(n - 1) * n;
            }
        }
        #endregion
    }

    struct Image
    {
        byte[] pixels;
        public Image(byte[] pixels)
        {
            this.pixels = pixels;
        }
        public byte[] getPixels()
        {
            return pixels;
        }
    }
}

﻿using System;

namespace _09_SurchargeOperateur
{
    class Program
    {
        static void Main(string[] args)
        {
            Point p1 = new Point(2, 4);
            Point pRes1 = -p1;
            Console.WriteLine($"p1={p1}");
            Console.WriteLine($"pRes1={pRes1}");

            Point p2 = new Point(1, 2);
            Point pRes2 = p1 + p2;
            Console.WriteLine($"pRes2={pRes2}");
            Point pRes3 = p1 * 4;
            Console.WriteLine($"pRes3={pRes3}");
            p2 += p1; // p2=p2+p1;   // quand on surcharge un opérateur, l'opérateur composé associé est aussi automatiquement surchargé

            Console.WriteLine($"p2={p2}");
            Console.WriteLine(p1 == p2);
            Console.WriteLine(p1 != p2);
            Point p3 = new Point(2, 4);
            Console.WriteLine(p1 == p3);

            Console.ReadKey();
        }
    }
}

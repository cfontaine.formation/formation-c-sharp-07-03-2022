﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Polymorphisme
{
    class Chat :Animal,IPeutMarcher
    {
        public int NbVie { get; set; } = 9;
        public Chat(double poid, int age,int nbVie) : base(poid, age)
        {
            NbVie = nbVie;
        }
        public override void EmettreSon()
        {
            Console.WriteLine("Le chat miaule");
        }

        public void Marcher()
        {
            Console.WriteLine("Le chat marche");
        }

        public void Courir()
        {
            Console.WriteLine("Le chat court");
        }
    }
}

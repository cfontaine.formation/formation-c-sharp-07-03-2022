﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Polymorphisme
{
    abstract class Animal
    {
        public double Poid { get; set; }
        public int Age { get; set; }

        public Animal(double poid, int age)
        {
            Poid = poid;
            Age = age;
        }

        //public virtual void EmettreSon()
        //{
        //    Console.WriteLine("L'animal émet un son");
        //}
        public abstract void EmettreSon();

        public override string ToString()
        {
            return $"Animal[{Age} {Poid}]";
        }
    }
}

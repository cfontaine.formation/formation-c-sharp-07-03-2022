﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Polymorphisme
{
    internal class Canard : Animal, IPeutMarcher, IPeutVoler
    {
        public Canard(double poid, int age) : base(poid, age)
        {
        }

        public override void EmettreSon()
        {
            throw new NotImplementedException();
        }
        public void Courir()
        {
            throw new NotImplementedException();
        }
        public void Marcher()
        {
            throw new NotImplementedException();
        }

        public void Decoller()
        {
            throw new NotImplementedException();
        }

        public void Atterir()
        {
            throw new NotImplementedException();
        }
    }
}

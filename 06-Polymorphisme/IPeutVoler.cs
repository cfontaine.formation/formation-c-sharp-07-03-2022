﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Polymorphisme
{
    interface IPeutVoler
    {
        void Decoller();

        void Atterir();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Polymorphisme
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Animal a1 = new Animal(4.5, 3);
            //a1.EmettreSon();
            //Console.WriteLine(a1);

            Chien ch1 = new Chien(6.0, 4, "Rolo");
            ch1.EmettreSon();
            Console.WriteLine(ch1);
            Animal a2= new Chien(4.5,5, "Laika");
            Console.WriteLine(a2.Poid + " " + a2.Age);
            a2.EmettreSon();

            //  Chien ch2 = (Chien) a2;

            //if (a2 is Chien) // tester si a2 est un Chien
            //{
            //    Chien ch2 = a2 as Chien;
            //    Console.WriteLine(ch2.Poid + " " + ch2.Age + " " + ch2.Nom);
            //    ch2.EmettreSon();
            //}

            if (a2 is Chien ch2) // tester si a2 est un Chien
            {
                Console.WriteLine(ch2.Poid + " " + ch2.Age + " " + ch2.Nom);
                ch2.EmettreSon();
            }

            Refuge r = new Refuge();
            r.Ajouter(ch1);
            r.Ajouter(a2);
            r.Ajouter(new Chat(3.5, 6, 9));
            r.Ajouter(new Chat(1.5, 1, 8));
            r.Ecouter();

            // TOstring

            // Equals
            Chien ch3= new Chien(6.0, 4, "Rolo");
            Chien ch4 = new Chien(6.0, 4, "Rolo");
            Console.WriteLine(ch3 == ch4); // Comparaison des références
            Console.WriteLine(ch3.Equals(ch4));


            //Interface
            IPeutMarcher ip1 = new Chien(3.6, 6, "Idefix");
            ip1.Courir();
            IPeutMarcher ip2 = new Chat(1.5, 1, 8);
            ip2.Courir();
            Console.ReadKey();
        }
    }
}

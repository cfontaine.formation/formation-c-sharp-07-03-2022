﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Polymorphisme
{
    class Labrador : Chien
    {
        public Labrador(double poid, int age, string nom) : base(poid, age, nom)
        {
        }

    }
}

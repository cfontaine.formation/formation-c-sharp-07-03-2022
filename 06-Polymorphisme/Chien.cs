﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Polymorphisme
{
    class Chien :Animal,IPeutMarcher
    {
        public string Nom { get; set; }
        public Chien(double poid, int age,string nom) : base(poid, age)
        {
            Nom = nom;
        }

        public sealed override void EmettreSon()
        {
            Console.WriteLine($"{Nom} aboie");
        }

        public override string ToString()
        {
            return $"Chien [{base.ToString()} {Nom}]" ;
        }

        public override bool Equals(object obj)
        {
            return obj is Chien chien &&
                   Poid == chien.Poid &&
                   Age == chien.Age &&
                   Nom == chien.Nom;
        }

        public override int GetHashCode()
        {
            int hashCode = -589909282;
            hashCode = hashCode * -1521134295 + Poid.GetHashCode();
            hashCode = hashCode * -1521134295 + Age.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Nom);
            return hashCode;
        }

        public void Marcher()
        {
            Console.WriteLine("Le chien marche");
        }

        public void Courir()
        {
            Console.WriteLine("Le chien court");
        }


        //public new void EmettreSon() //new -> occultation considéré comme une nouvelle méthode
        //{                            // indépendante de celle de la classe Mère
        //    Console.WriteLine($"{Nom} aboie");
        //}




    }
}

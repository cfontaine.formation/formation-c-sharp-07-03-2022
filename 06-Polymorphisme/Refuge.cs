﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Polymorphisme
{
    class Refuge
    {
        public Animal[] Place= new Animal[20];

        private int nbPlaceDispo = 20;

        public void Ajouter(Animal a)
        {
            if (nbPlaceDispo > 0)
            {
                Place[20 - nbPlaceDispo] = a;
                nbPlaceDispo--;
            } 
        }
 
        public void Ecouter()
        {
            for(int i=0;i< 20 - nbPlaceDispo; i++)
            {
                Place[i].EmettreSon();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
  /*sealed*/ class Voiture
    {
        // Variables d'instances => Etat
       
        string _couleur;     // Variable d'instance utilisée par la propriété Couleur (C# 7.0)
        string _plaqueIma;  // Variable d'instance utilisée par la propriété PlaqueIma (propriété avec une condition)
        // On n'a plus besoin de déclarer les variables d'instances _marque,_vitesse, _compteurKm, elles seront générées automatiquement par les propriétées automatique
        // string _marque = "Opel";
        // int _vitesse;
        // int _compteurKm = 200;

        // Propriété auto-implémenté => la variable d'instance est générée par le compilateur
        public int Vitesse { get; protected set; } // On peut associer un modificateur d'accès à get et à set. Il doit être plus restritif que le modificateur de la propriété
        public int CompteurKm { get;  } = 200; // On peut donner un valeur par défaut à une propriétée (littéral, expression ou une fonction)
        public string Marque { get; } = "Opel";

        // Variable de classe => variable partagée par tous les objets 
        // static int _compteurVoiture; // public static int nbVoiture;     // Remplacer par une propriétée static
        public static int CompteurVoiture { get; private set; }

        // Agrégation
        public Personne Proprietaire { get; set; }

        // Getter/Setter C++/Java =>  en C# on utilisera les propriétés
        // Getter (accès en lecture)
        //public string GetPlaqueIma()
        // {
        //     return _plaqueIma;
        // }

        // Setter (accès en écriture)
        // public void SetPlaqueIma(string plaqueIma)
        // {
        //     if (plaqueIma != null)
        //     {
        //         _plaqueIma = plaqueIma;
        //     }
        // }
        // propriété
        public string PlaqueIma
        {
            get
            {
                return _plaqueIma;
            }
            set
            {
                if (value != null)
                {
                    _plaqueIma = value;
                }
            }

        }

        // Propriété c#7.0 get et set d'une seule ligne
        public string Couleur
        {
            get => _couleur;
            // set => _couleur = value!=null ? value:_couleur;
            //ou
            set
            {
                if (value != null)
                {
                    _couleur = value;
                }
            }
        }

        // Constructeurs => On peut surcharger le constructeur
        // Constructeur par défaut
        public Voiture()
        {
            CompteurVoiture++;
            Console.WriteLine("Constructeur par défaut Voiture");
        }
        // this() => Chainnage de constructeur : appel du constructeur par défaut
        public Voiture(string marque, string couleur, string plaqueIma) : this()
        {
            Console.WriteLine("Constructeur 3 paramètres");
            Marque = marque;
            this._couleur = couleur;
            this._plaqueIma = plaqueIma;
        }

        // this(marque, couleur, plaqueIma) => Chainnage de constructeur : appel du constructeur Voiture(string marque,string couleur, string plaqueIma)
        public Voiture(string marque, string couleur, string plaqueIma, int vitesse, int compteurKm) : this(marque, couleur, plaqueIma)
        {
            Console.WriteLine("Constructeur 5 paramètres");
            // this._vitesse = vitesse;
            Vitesse = vitesse;
            CompteurKm = compteurKm;
        }

        public Voiture(string marque, Personne proprietaire, string plaqueIma, string couleur)
        {
            Marque = marque;
            Proprietaire = proprietaire;
            PlaqueIma = plaqueIma;
            Couleur = couleur;
        }

        // Destructeur => libérer des ressources
        //~Voiture()
        //{
        //    compteurVoiture--;
        //    Console.WriteLine("Destructeur voiture");
        //}

        // Constructeur statique: appelé avant la création de la première instance ou le référencement d’un membre statique
        //static Voiture()
        //{
        //    Console.WriteLine("------------>Constructeur statique");
        //}




        // Méthodes d'instances => comportement
        public void Accelerer(int vAcc)
        {
            if (vAcc > 0)
            {
                //_vitesse += vAcc;
                Vitesse += vAcc;
            }

        }

        public void Freiner(int vFrn)
        {
            if (vFrn > 0)
            {
                //  _vitesse -= vFrn;
                Vitesse -= vFrn;
            }
        }

        public void Arreter()
        {
            //_vitesse = 0;
            Vitesse = 0;
        }

        public bool EstArreter()
        {
            return Vitesse == 0;
        }

        public void Afficher()
        {
            //Console.WriteLine("Méthode Voiture");
            Console.WriteLine($"marque={Marque} vitesse={Vitesse} KM={CompteurKm}");
            //Proprietaire?.Afficher();
            if (Proprietaire != null)
            {
                Proprietaire.Afficher();
            }
            else
            {
                Console.WriteLine("Pas de proprietaire");
            }
        }

        // Méthode de classe
        public static void TestMethodeClasse()
        {
            Console.WriteLine("Méthode de classe"); 
            Console.WriteLine(CompteurVoiture); // On peut accéder dans une méthode de classe à une variable de classe ou à une autre méthode de classe
            // Console.WriteLine(vitesse);     // Dans une méthode de classe, on n'a pas accès à une variable d'instance
            // Freiner(12);                    // ou une méthode d'instance
        }

        public static bool EgalVitesse(Voiture a,Voiture b)
        {
            return a.Vitesse == b.Vitesse;  // mais on peut accéder à une variable d'instance par l'intermédiare d'un objet passé en paramètre
        }
    }
}

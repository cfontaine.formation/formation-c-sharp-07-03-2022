﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
    class CompteEpargne : CompteBancaire
    {
        public decimal Taux { get; set; } = 1.5M;

        public CompteEpargne()
        {
        }

        public CompteEpargne(decimal taux) 
        {
            Taux = taux;
        }

        public CompteEpargne(Personne titulaire) : base(titulaire)
        {
        }

        public CompteEpargne(decimal taux,Personne titulaire) : base(titulaire)
        {
            Taux = taux;
        }

        public void CalculInterets()
        {
            Solde = Solde * (1 + Taux / 100);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
    class Personne
    {
        public string Prenom { get; set; }
        public string Nom { get; set; }
        public string Telephone { get; set; }

        public Personne(string prenom, string nom, string telephone)
        {
            Prenom = prenom;
            Nom = nom;
            Telephone = telephone;
        }

        public void Afficher() 
        {
            Console.WriteLine($" {Prenom} {Nom} {Telephone}");
        }
    }
}

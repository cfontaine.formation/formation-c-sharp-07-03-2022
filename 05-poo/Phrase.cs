﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
    class Phrase
    {
        string[] _mots;

        public Phrase(params string[] mots)
        {
            _mots = mots;
        }

        public string this[int index]
        {
            get
            {
                return _mots[index];
            }
            set
            {
                _mots[index] = value;
            }
        }

        public int this[string mot]
        {
            get
            {
                for(int i = 0; i < _mots.Length; i++)
                {
                    if(_mots[i]==mot)
                    {
                        return i;
                    }
                }
                throw new IndexOutOfRangeException();
            }
            
        }

        public void Afficher()
        {
            for(int i = 0; i < _mots.Length; i++)
            {
                Console.Write($"{_mots[i]} ");
            }
        }
    }
}

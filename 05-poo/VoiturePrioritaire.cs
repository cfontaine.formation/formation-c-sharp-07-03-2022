﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
    class VoiturePrioritaire : Voiture
    {
        public bool Gyro { get; private set; }

        public VoiturePrioritaire() //: base() -> implicite
        {
        } 
        public VoiturePrioritaire(bool gyro) : base("Subaru","rouge","aa-1234-UI") // -> explicite
        {
            Gyro = gyro;
        }

        public VoiturePrioritaire(string marque, string couleur, string plaqueIma,bool gyro) : base(marque, couleur, plaqueIma)
        {
            Gyro = gyro;
        }

        public void AllumerGyro()
        {
            Gyro = true;
            Vitesse = Vitesse * 2;
        }

        public void EteindreGyro()
        {
            Gyro = true;
        }

        public void Accelerer(int vAcc)
        {
            base.Accelerer(2 * vAcc);
        }

        public void Afficher()
        {
            base.Afficher(); // appel de la méthode afficher de la classe mère Voiture
           // Console.WriteLine("Méthode Voiture Prioritaire");
            Console.WriteLine($"gyro={Gyro}");
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
    class Container
    {
        private static string vClasse = "Variable de classe";
        private string vInstance = "Variable d'instance";

        public void Test()
        {
            Element elm = new Element();
            elm.TestVariableClasse();
            elm.TestVariableInstance(this);
        }

        // 
        private class Element
        {

            public void TestVariableClasse()
            {
                Console.WriteLine(vClasse);
            }

            public void TestVariableInstance(Container c)
            {
                Console.WriteLine(c.vInstance);
            }
        }
    }
}

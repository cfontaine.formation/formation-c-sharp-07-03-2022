﻿using System;

namespace _05_poo
{
    class CompteBancaire
    {

        public decimal Solde { get; protected set; } = 50.0M;
        public string Iban { get; }

        public Personne Titulaire { get; set; } //string

        static int compteurCompte;

        public CompteBancaire()
        {
            compteurCompte++;
            Iban = "fr-5962-0000-" + compteurCompte;
        }

        public CompteBancaire(Personne titulaire) : this()
        {
            //compteurCompte++;
            //iban = "fr-5962-0000-" + compteurCompte;
            Titulaire = titulaire;
        }

        public CompteBancaire(decimal solde, Personne titulaire) : this(titulaire)
        {
            //compteurCompte++;
            //iban = "fr-5962-0000-" + compteurCompte;
            //this.titulaire = titulaire;
            Solde = solde;
        }

        public void Crediter(decimal valeur)
        {
            if (valeur > 0)
            {
                Solde += valeur;
            }
        }

        public void Debiter(decimal valeur)
        {
            if (valeur > 0)
            {
                Solde -= valeur;
            }
        }

        public bool EstPositif()
        {
            return Solde > 0;
        }

        public void Afficher()
        {
            Console.WriteLine("________________________");
            Console.WriteLine($"solde= {Solde}");
            Console.WriteLine($"iban= {Iban}");
            Console.Write("titulaire= ");
            Titulaire?.Afficher();
            Console.WriteLine("________________________");
        }
    }
}

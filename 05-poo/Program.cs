﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Voiture
            // Console.WriteLine("***************************");
            Voiture.TestMethodeClasse();

            // Appel d'une variable de classe
            Console.WriteLine($"nombre voiture={Voiture.CompteurVoiture}");

            // Instantiation de la classe Voiture
            Voiture v1 = new Voiture();
            Console.WriteLine($"nombre voiture={Voiture.CompteurVoiture}");
            v1.Afficher();
            // v1.Marque = "Ford";
            // v1.Vitesse = 10; // erreur: pas set dans la propriété Vitesse

            // Accès à une propriété en lecture (get)
            Console.WriteLine(v1.Marque);
            Console.WriteLine(v1.PlaqueIma); //v1._plaqueIma*
            Console.WriteLine(v1.Vitesse);

            // Accès à une propriété en ecriture (set)
            v1.PlaqueIma = null;//v1.SetPlaqueIma(null);
            v1.PlaqueIma = "FR-34000-AB";//v1.SetPlaqueIma("FR-34000-AB");
            Console.WriteLine(v1.PlaqueIma);

            // Appel d’une méthode d’instance
            v1.Accelerer(10);
            v1.Afficher();
            v1.Freiner(5);
            v1.Afficher();
            Console.WriteLine(v1.EstArreter());
            v1.Arreter();
            v1.Afficher();
            Console.WriteLine(v1.EstArreter());


            Voiture v2 = new Voiture();
            Console.WriteLine($"nombre voiture={Voiture.CompteurVoiture}");
           // v2.Marque = "honda";
           // v2._vitesse = 30;
            Console.WriteLine(v2.Vitesse);

            Console.WriteLine(Voiture.EgalVitesse(v1, v2));


            Voiture v3 = new Voiture("Toyota", "Rouge", "fr-1246-AB");
            Console.WriteLine($"nombre voiture={Voiture.CompteurVoiture}");
            v3.Afficher();
            // Test de l'appel du destructeur
            v3 = null; // En affectant, null à la références v3.Il n'y a plus de référence sur l'objet voiture
                       // Il sera détruit lors de la prochaine execution du garbage collector
                       //   GC.Collect();  // Forcer l'appel du garbage collector

            // Initialiseur d'objet
            // Avec un initialiseur d'objet on a accès uniquement à ce qui est public
            string str = "Blanc";
            Voiture v4 = new Voiture {/*Marque="Ford",*/ Couleur = str };
            Console.WriteLine($"nombre voiture={Voiture.CompteurVoiture}");
            v4.Afficher();

            Voiture v5 = new Voiture("Fiat", "jaune", "fr-AZER-62", 25, 1000);
            Console.WriteLine($"nombre voiture={Voiture.CompteurVoiture}");
            v5.Afficher();
            Console.WriteLine(Voiture.EgalVitesse(v1, v5));
       
            #endregion

            #region Indexer
            Phrase phr = new Phrase("Il", "fait", "beau", "aujourd'hui");
            Console.WriteLine(phr[1]);
            Console.WriteLine(phr["aujourd'hui"]);
            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine($"{phr[i]}" );
            }
            phr.Afficher();
            #endregion

            // Classe imbriqué
            Container container = new Container();
            container.Test();

            // Classe Statique
            Console.WriteLine(Math.Min(2, 4));
            Console.WriteLine(Math.PI);
            //Math ma = new Math();

            // Méthode d'extension
            string str1 = "HelloWorld";
            Console.WriteLine(str1.Inverse());

            // Agrégation
            Personne per1 = new Personne("John", "Doe", "06.00.00.00");
            v1.Proprietaire = per1;
            v1.Afficher();

            Voiture v6 = new Voiture("Ford", new Personne("Allan", "Smithee", "07.00.00.00"), "fr-3333-RT", "Bleu");
            v6.Afficher();

            // Héritage
            VoiturePrioritaire vp1 = new VoiturePrioritaire();
            vp1.Afficher();
            Console.WriteLine(vp1.Gyro);
            vp1.AllumerGyro();
            Console.WriteLine(vp1.Gyro);

            VoiturePrioritaire vp2 = new VoiturePrioritaire("Volvo", "vert", "fr-3456-TY", true);
            vp2.Afficher();


            CompteBancaire cb1 = new CompteBancaire(100.0M, per1);// "John Doe"
            // cb1.solde = 100.0M;
            // cb1.iban = "fr5962-00000";
            //  cb1.titulaire = "John Doe";
            cb1.Crediter(50.0M);
            cb1.Debiter(20.0M);
            cb1.Afficher();
            Personne per2 = new Personne("Jane", "Doe", "06.00.00.00");
            CompteBancaire cb2 = new CompteBancaire(per2);
            cb2.Afficher();
            Console.WriteLine($"nombre voiture={Voiture.CompteurVoiture}");

            CompteEpargne ce1 = new CompteEpargne(0.5M,per2);
            ce1.Crediter(300.0M);
            ce1.Afficher();
            ce1.CalculInterets();
            ce1.Afficher();


            //Point

            Point p1 = new Point(1, 3);
            Point p2 = new Point(3, 1);
            p1.Afficher();
            p2.Afficher();
            p2.Deplacer(1, 2);
            p2.Afficher();
            Console.WriteLine($"Norme={p1.Norme()}");
            Console.WriteLine($"Distance={Point.Distance(p1,p2)}");

            Console.ReadKey();
        }
    }
}

﻿namespace _05_poo
{
    static class Extension
    {
        public static string Inverse(this string str)
        {
            string tmp = "";
            for (int i = str.Length - 1; i >= 0; i--)
            {
                tmp += str[i];
            }
            return tmp;
        }
    }
}

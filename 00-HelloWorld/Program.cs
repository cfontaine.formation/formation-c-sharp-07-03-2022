﻿using System;

namespace _00_HelloWorld
{
    /// <summary>
    /// Le programme hello world
    /// </summary>
    class Program
    {
        /// <summary>
        /// Point d'entrée du programme
        /// </summary>
        /// <param name="args">Arguments de la ligne de commande</param>
        static void Main(string[] args) // Commentaire fin de ligne
        {
            /*
             Commentaire
             sur 
             plusieurs lignes */
            Console.WriteLine("Hello world!");

            // Console.ReadKey() => pour attendre l'appuie d'une touche, sinon la console se ferme 
            Console.ReadKey();
        }
    }
}

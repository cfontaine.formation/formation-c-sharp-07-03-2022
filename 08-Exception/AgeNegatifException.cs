﻿using System;
// Exception personnalisée
namespace _08_Exception
{
    internal class AgeNegatifException : Exception
    {
        public int Age { get; set; }
        public AgeNegatifException(int age)
        {
            Age = age;
        }

        public AgeNegatifException(string message, int age) : base(message)
        {
            Age = age;
        }

        public AgeNegatifException(string message, Exception innerException, int age) : base(message, innerException)
        {
            Age = age;
        }
    }
}

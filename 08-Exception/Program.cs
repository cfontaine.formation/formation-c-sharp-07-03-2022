﻿using System;
namespace _08_Exception
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int[] t = new int[5];
                Console.WriteLine("Entrer un indice de l'élément du tableau");
                int index = Convert.ToInt32(Console.ReadLine());    // Peut générer une Exception FormatException, si on saisie autre chose qu'un nombre entier
                Console.WriteLine(t[index]); // Peut générer une Exception IndexOutOfRangeException , si index est > à Length
                Console.WriteLine("Entrer un age");
                int age = Convert.ToInt32(Console.ReadLine());   // Peut générer une Exception FormatException, si on saisie autre chose qu'un nombre entier
                TraitememtEmploye(age);
                Console.WriteLine("Suite du programme");
            }
            catch (IndexOutOfRangeException e)  // Attrape les exceptions IndexOutOfRangeException
            {
                Console.WriteLine($"Index en dehors des limites du tableau message={e.Message}");    // Message => permet de récupérer le messsage de l'exception
                Console.WriteLine(e.StackTrace);
            }
            catch (FormatException e)    // Attrape les exceptions FormatException
            {
                Console.WriteLine("Mauvais format");
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            catch (ArgumentException e)  // Attrape les exceptions ArgumentException
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.InnerException.Message);
                Console.WriteLine(e.InnerException.StackTrace);
            }
            catch (Exception e) // Attrape tous les autres exception
            {
                Console.WriteLine("Traitement global");
                Console.WriteLine(e.Message);
            }
            finally // Le bloc finally est toujours éxécuté
            {
                // finally est utiliser pour libérer les ressources
                Console.WriteLine("toujours exécuté");
            }
            Console.WriteLine("Fin du programme");
            Console.ReadKey();
        }

        static void TraitememtEmploye(int age)
        {
            Console.WriteLine("Début du traitement de l'employé");
            try
            {
                TraitementAge(age);
            }
            catch (AgeNegatifException e) when (e.Age > -10)    // when : le catch est éxécuté si la condition dans when est vrai
            {
                Console.WriteLine($"traitement Local >-10: {e.Message} {e.Age}");
                // On relance une autre exception
                throw new ArgumentException($"Erreur traitement emeployé age {e.Age}", e);   // e =>référence à l'exception qui a provoquer l'exception
            }
            catch (AgeNegatifException e) when (e.Age < -40)
            {
                // Traitement partiel de l'exception AgeNegatifException
                Console.WriteLine($"traitement Local <-40: {e.Message} {e.Age}");
                throw; // On relance l'exception pour que l'utilisateur de la méthode la traite a son niveau
            }
            Console.WriteLine("Fin du traitement de l'employé");
        }

        static void TraitementAge(int age)
        {
            Console.WriteLine("Début du traitement de l'age");
            if (age < 0)
            {
                throw new AgeNegatifException("Age négatif", age);    //  throw => Lancer un exception
            }                                                        //  Elle va remonter la pile d'appel des méthodes jusqu'à ce qu'elle soit traité par un try/catch
                                                                     //  si elle n'est pas traiter, aprés la méthode main => arrét du programme
            Console.WriteLine("Fin du traitement de l'age");
        }
    }
}

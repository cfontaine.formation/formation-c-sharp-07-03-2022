﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19_ExoSuplementaire
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Permutation de 2 variables 
            // Saisir deux variables de type entier et les permuter avant de les afficher
            Console.WriteLine("Saisir 2 entiers");
            int vp1=Convert.ToInt32(Console.ReadLine());
            int vp2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"vp1={vp1} vp2={vp2}");

            int tmp = vp1;
            vp1 = vp2;
            vp2 = tmp;

            Console.WriteLine($"vp1={vp1} vp2={vp2}");
            #endregion

            #region Permutation de 2 octets
            // Permutez les deux octets d'une variable entière de 2 octets saisis par l'utilisateur
            // Afficher la valeur avant et après la pemmutation en hexadécimal et en binaire
            Console.WriteLine("Saisir un entiers (entre 0 et 65 536)");
            short val2Octet =Convert.ToInt16(Console.ReadLine());
            Console.WriteLine($"{Convert.ToString((val2Octet), 16)}");
            Console.WriteLine($"{Convert.ToString((val2Octet&0xFF00)>>8,2)}  {Convert.ToString(val2Octet & 0x00FF, 2)}");

            val2Octet = (short)((val2Octet << 8) | (val2Octet >> 8));

            Console.WriteLine($"{Convert.ToString((val2Octet & 0xFF00) >> 8, 2)}  {Convert.ToString(val2Octet & 0x00FF, 2)}");
            Console.WriteLine($"{Convert.ToString((val2Octet), 16)}");
            #endregion

            #region Trie de 3 Valeurs
            // Saisir 3 nombres et afficher ces nombres triés dans l'ordre croissant sous la forme 1.5 __<__ 10.5 __<__ 50.7
            Console.WriteLine("Saisir 3 nombres décimaux");
            double vt1 = Convert.ToDouble(Console.ReadLine());
            double vt2 = Convert.ToDouble(Console.ReadLine());
            double vt3 = Convert.ToDouble(Console.ReadLine());
            if(vt2 < vt1 )
            {
                double vTmp = vt1;
                vt1= vt2;
                vt2 = vTmp;
            } // => vt1<vt2
            if(vt3 < vt1)
            {
                Console.WriteLine($"{vt3} < {vt1} < {vt2}");
            }else if(vt3>vt2)
            {
                Console.WriteLine($"{vt1} < {vt2} < {vt3}");
            }
            else
            {
                Console.WriteLine($"{vt1} < {vt3} < {vt2}");
            }
            #endregion

            #region Paire
            // Créer un programme qui indique, si le nombre entier saisie dans la console est __paire__ ou __impaire__
            Console.WriteLine("Saisir un nombre entier");
            int vi= Convert.ToInt32(Console.ReadLine());
            if (vi % 2 == 0) // ou (vi & 1 ==0)
            {
                Console.WriteLine("Paire");
            }
            else
            {
                Console.WriteLine("Impaire");
            }
            #endregion


            #region Valeur absolue
            // Saisir un chiffre décimal et afficher la valeur absolue sous la forme __ | __ - 1.85 __ | = __ 1.85
            Console.WriteLine("Saisir une  nombre décimal");
            double va = Convert.ToDouble(Console.ReadLine());
            double vAbs = va < 0 ? -va : va;
            Console.WriteLine($"|{va}|={vAbs}");
            #endregion

            #region Afficher les nombres d'un intervalle
            //Saisir 2 nombres entier et afficher tous les nombres entiers entre le nombre saisi le plus grand et le plus petit
            // 23 26 donne 26 25 24 23
            // 34 31 donne 34 33 32 31
            Console.WriteLine("Saisir 2 entiers");
            int vi1 = Convert.ToInt32(Console.ReadLine());
            int vi2 = Convert.ToInt32(Console.ReadLine());
            if(vi1 < vi2)
            {
                for(int i=vi1;i<=vi2;i++)
                {
                    Console.Write($"{i} ");
                }
            }
            else
            {
                for (int i = vi1; i >= vi2; i--)
                {
                    Console.Write($"{i} ");
                }
            }
            Console.WriteLine("");
            #endregion
            #region Table de multiplication
            // Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9

            //  1 X 4 = 4
            //  2 X 4 = 8
            //    …
            //  9 x 4 = 36
            // Si le nombre passé en paramètre est en dehors de l’intervalle 1 à 9 on arrête sinon on redemande une nouvelle valeur

            for (; ; ) // boucle infinie
            {
                Console.WriteLine("Saisir un nombre entre 1 et 9 pour afficher sa table de multiplication");
                int m = Convert.ToInt32(Console.ReadLine());
                if(m<1 || m>9)
                {
                    break;
                }
                for(int i=1;i<=9;i++)
                {
                    Console.WriteLine($"{i} x {m} = {i * m}");
                }
            }

            #endregion
            Console.ReadKey();


        }
    }
}

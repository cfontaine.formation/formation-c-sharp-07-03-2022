﻿using _14C_BibliothequeBddGeneric;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace _14A_BibliothequeBdd
{
    public static class ContactExportImport // static -> Uniquement des méthodes de le classe
    {
       public static void ExportCsv(string chemin, char separateur, List<Contact> contacts)
        {
            using (StreamWriter sw = new StreamWriter(chemin))
            {
                sw.WriteLine($"id{separateur}prenom{separateur}nom{separateur}date_naissance{separateur}email");
                foreach (var contact in contacts)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(contact.Id);
                    sb.Append(separateur);
                    sb.Append(contact.Prenom);
                    sb.Append(separateur);
                    sb.Append(contact.Nom);
                    sb.Append(separateur);
                    sb.Append(contact.DateNaissance);
                    sb.Append(separateur);
                    sb.Append(contact.Email);
                    sw.WriteLine(sb.ToString());
                }
            }
        }
        public static List<Contact> ImportCsv(string chemin, char separateur)
        {
            List<Contact> contacts = new List<Contact>();
            using (StreamReader sw = new StreamReader(chemin))
            {
                sw.ReadLine();
                while (true)
                {
                    string ligne = sw.ReadLine(); // Ignorer la première ligne du fichierS
                    if (ligne == null)
                    {
                        break;
                    }
                    string[] elm = ligne.Split(separateur);
                    if (elm.Length != 5)
                    {
                        throw new Exception("Le format du fichier .csv est incorrecte");
                    }
                    Contact c = new Contact(elm[1], elm[2], DateTime.Parse(elm[3]), elm[4]);
                    c.Id = Convert.ToInt64(elm[0]);
                    contacts.Add(c);
                }
                return contacts;
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace _14C_BibliothequeBddGeneric
{
    public class ContactDao : GenericDao<Contact>
    {
        protected override void Create(SqlConnection cnx, Contact contact)
        {
            string req = "INSERT INTO contacts(prenom,nom,jour_naissance,email)VALUES (@prenom,@nom,@jourNaissance,@email); ";
            SqlCommand cmd = new SqlCommand(req, cnx);
            cmd.Parameters.AddWithValue("@prenom", contact.Prenom);
            cmd.Parameters.AddWithValue("@nom", contact.Nom);
            cmd.Parameters.AddWithValue("@jourNaissance", contact.DateNaissance);
            cmd.Parameters.AddWithValue("@email", contact.Email);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            //contact.Id = cmd.LastInsertedId;
        }

        protected override void Delete(SqlConnection cnx, Contact contact)
        {
            string req = "DELETE FROM contacts WHERE id=@id";
            SqlCommand cmd = new SqlCommand(req, cnx);
            cmd.Parameters.AddWithValue("@id", contact.Id);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }

        protected override void Update(SqlConnection cnx, Contact contact)
        {
            string req = "UPDATE contacts SET nom = @nom, prenom=@prenom, date_naissance=@dateNaissance, email=@email  WHERE id=@id";
            SqlCommand cmd = new SqlCommand(req, cnx);
            cmd.Parameters.AddWithValue("@id", contact.Id);
            cmd.Parameters.AddWithValue("@prenom", contact.Prenom);
            cmd.Parameters.AddWithValue("@nom", contact.Nom);
            cmd.Parameters.AddWithValue("@dateNaissance", contact.DateNaissance);
            cmd.Parameters.AddWithValue("@email", contact.Email);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }

        protected override Contact Read(SqlConnection cnx, long id)
        {
            Contact contact = null;
            string req = "SELECT id,prenom,nom,date_naissance,email FROM contacts  WHERE id=@id";
            SqlCommand cmd = new SqlCommand(req, cnx);
            cmd.Parameters.AddWithValue("@id", id);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                contact = new Contact(reader.GetString(1), reader.GetString(2), reader.GetDateTime(3), reader.GetString(4));
                contact.Id = reader.GetInt64(0);
            }
            reader.Close();
            cmd.Dispose();
            return contact;
        }

        protected override List<Contact> ReadAll(SqlConnection cnx)
        {
            List<Contact> lst = new List<Contact>();
            string req = "SELECT id,prenom,nom,date_naissance,email FROM contacts";
            SqlCommand cmd = new SqlCommand(req, cnx);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Contact c = new Contact(reader.GetString(1), reader.GetString(2), reader.GetDateTime(3), reader.GetString(4));
                c.Id = reader.GetInt64(0);
                lst.Add(c);
            }
            reader.Close();
            cmd.Dispose ();
            return lst;
        }

        // Dans le dao, on peut déclarer d'autre méthodes suplémentaires uniquement pour la classe Contact 
        public bool IsEmailExist(string email, bool close = true)   // test si un email existe dans la base de donnée
        {
            bool exist = false;
            SqlConnection cnx = GetConnection();
            string req = "SELECT email FROM contacts WHERE email=@email";
            SqlCommand cmd = new SqlCommand(req, cnx);
            cmd.Parameters.AddWithValue("@email", email);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                exist = true;
            }
            reader.Close();
            cmd.Dispose();
            CloseConnection(close);
            return exist;
        }

        public Contact FindByEmail(string email, bool close = true)    // Trouve un contact dans la bdd en fonction d'un email 
        {
            Contact c = null;
            SqlConnection cnx = GetConnection();
            string req = "SELECT id,prenom,nom,date_naissance,email FROM contacts WHERE email=@email";
            SqlCommand cmd = new SqlCommand(req, cnx);
            cmd.Parameters.AddWithValue("@email", email);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                c = new Contact(reader.GetString(1), reader.GetString(2), reader.GetDateTime(3), email);
                c.Id = reader.GetInt64(0);
            }
            reader.Close();
            cmd.Dispose();
            CloseConnection(close);
            return c;
        }

    }
}

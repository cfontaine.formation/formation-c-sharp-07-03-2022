﻿namespace _14C_BibliothequeBddGeneric
{
    // La classe abstract DbObject est la classe mère des objets persistés dans la base de données
    // Elle gère l'id qui fait le lien entre l'objet et la table des la bdd (clé primaire) 
    public abstract class DbObject  // par défaut une classe a pour visiblité internal, elle n'est pas visible en dehors de l'assenbly (dll)
    {                               // pour qu'elle soit visible en dehors il faut qu la visibilité soit public                    
        public long Id { get; set; }

        // On compare les objets par rapport à leur id (2 objets avec un id égal sont égaux)
        public override bool Equals(object obj)
        {
            return obj is DbObject @object &&
                   Id == @object.Id;
        }

        public override int GetHashCode()
        {
            return 2108858624 + Id.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format($"{Id}");
        }
    }

}

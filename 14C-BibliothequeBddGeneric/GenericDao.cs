﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace _14C_BibliothequeBddGeneric
{
    // La classe abstraite GenericDao est la classe mère de tous les dao
    // Elle permet de gérer la connection à la base de données et fournit les méthodes Create Read Update Delete 
    public abstract class GenericDao<T> where T : DbObject  // le type générique T ne peut être qu'une classe qui hérite de DbObject
    {
        public static string ChaineConnection { get; set; }

        protected static SqlConnection cnx;

        ~GenericDao()
        {
            CloseConnection(true);
        }

        // Méthode pour persiter un objet ou le mettre à jour s'il existe dans la base de donnée
        // close à false permet de ne pas fermer la connection à la base de donnée
        public void SaveOrUpdate(T elm, bool close = true)
        {
            if (elm.Id == 0)
            {
                Create(GetConnection(), elm);
            }
            else
            {
                Update(GetConnection(), elm);
            }
            CloseConnection(close);
        }

        // Méthode pour supprimer un objet de la base de données
        public void Delete(T elm, bool close = true)
        {
            Delete(GetConnection(), elm);
            CloseConnection(close);
        }

        // Méthode pour récupérer un objet dans la base de donnée en fonction de son id
        public T FindById(long id, bool close = true)
        {
            T tmp = Read(GetConnection(), id);
            CloseConnection(close);
            return tmp;
        }

        // Méthode pour récupérer tous les objets de la base de donnée
        public List<T> FindAll(bool close = true)
        {
            List<T> lstTmp = ReadAll(GetConnection());
            CloseConnection(close);
            return lstTmp;
        }

        // Méthode qui permet d'obtenir la connection à la base de donnée
        protected SqlConnection GetConnection()
        {
            if (cnx == null)
            {
                cnx = new SqlConnection(ChaineConnection);
                cnx.Open();
            }
            return cnx;
        }

        // Méthode qui permet de fermer la connection à la base de donnée
        protected void CloseConnection(bool close)
        {
            if (cnx != null && close)
            {
                cnx.Close();
                cnx.Dispose();
                cnx = null;
            }
        }

        // Méthodes abstraites qui vont contenir les requêtes SQL dans les sous-classes
        protected abstract void Create(SqlConnection cnx, T elm);
        protected abstract void Update(SqlConnection cnx, T elm);
        protected abstract void Delete(SqlConnection cnx, T elm);
        protected abstract T Read(SqlConnection cnx, long id);
        protected abstract List<T> ReadAll(SqlConnection cnx);
    }

}


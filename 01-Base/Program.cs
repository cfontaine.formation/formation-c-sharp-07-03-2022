﻿using System;
using System.Text;

namespace _01_Base
{
    // Une enumération est un ensemble de constante
    // Par défaut, une énumération est de type int , on peut définir un autre type qui ne peut-être que de type intégral
    enum Direction { NORD = 90, SUD = 270, EST = 0, OUEST = 180 } // par défaut int
    enum Motorisation : short { ESSENCE, GPL, DIESEL, HYBRIDE, ELECTRIQUE }

    [Flags]
    enum Jours
    {
        LUNDI = 1,
        MARDI = 2,
        MERCREDI = 4,
        JEUDI = 8,
        VENDREDI = 16,
        SAMEDI = 32,
        DIMANCHE = 64,
        WEEKEND = SAMEDI | DIMANCHE
    }

    // Définition d'une structure
    struct Point
    {
        public int X;
        public int Y;
    }
    class Program
    {
        static void Main(string[] args)
        {
            #region Variable et types simple
            // Déclaration d'une variable   type nomVariable;
            int i;

            //Console.WriteLine(i); // Erreur => on ne peut pas utiliser une variable locale non initialisée

            // Initialisation    nomVariable = valeur
            i = 42;
            Console.WriteLine(i);

            // Déclaration multiple de variable
            double hauteur, largeur;
            hauteur = 3.4;
            largeur = 45.6;
            Console.WriteLine(hauteur + " largeur=" + largeur);// + => concaténation

            // Déclaration et initialisation de variable    type nomVariable = valeur;
            int j = 12;
            int j1 = 4, j2 = 45;
            Console.WriteLine(j + " " + j1 + " " + j2);
            #endregion
            // Littéral booléen
            bool test = true; //false
            Console.WriteLine(test);
            // Littéral caratère
            char chr = 'a';
            char chrUtf8 = '\u0045';    // Caractère en UTF-8
            char chrUtf8Hexa = '\x45';
            Console.WriteLine(chr + " " + chrUtf8 + " " + chrUtf8Hexa);

            // Littéral entier -> int par défaut
            long l = 12L; // L -> Long
            uint ui = 12U; // U -> unsigned
            Console.WriteLine(l + " " + ui);

            // Littéral entier -> changement de base
            int dec = 10;           // décimal (base 10) par défaut
            int hexa = 0xFF2;       // 0x => héxadécimal (base 16)
            int bin = 0b010101001;  // 0b => binaire (base 2)
            Console.WriteLine(dec + " " + hexa + " " + bin);

            // Littéral nombre à virgule flottante
            double d3 = 100.4;
            double exp = 3.5e3;
            Console.WriteLine(d3 + " " + exp);

            // Littéral nombre à virgule flottante => par défaut double
            float f = 10.2F; // F -> float
            decimal deci = 12.3M; // M -> décimal
            Console.WriteLine($"{f} {deci}");

            // Type implicite -> var
            // type est déterminer par le type de la littérale, de l'expression ou du retour d'une méthode
            var v1 = 10;         // v1 -> double
            var v2 = "azerty";   // v2 -> string
            var v3 = i + j;      // v3 -> int le résultat de i + j est un entier
            Console.WriteLine(v1 + " " + v2 + " " + v3);

            // avec @ on peut utiliser les mots réservés comme nom de variable (uniquement si nécessaire)
            int @while = 12;
            Console.WriteLine(@while);

            #region Transtypage
            // Transtypage implicite: ( pas de perte de donnée)
            // type inférieur => type supérieur
            int tr1 = 34;
            long tr2 = tr1;
            double tr3 = tr1;
            Console.WriteLine(tr1 + " " + tr2 + " " + tr3);

            // Transtypage explicite: cast -> (nouveauType)
            double te1 = 3.45;
            int te2 = (int)te1;
            Console.WriteLine(te1 + " " + te2);
            decimal te3 = (decimal)te1;
            Console.WriteLine(te1 + " " + te3);
            #endregion

            #region dépassement de capacité
            int sh1 = 300;              // 00000000 00000000 00000001 00101100    300
            sbyte sb1 = (sbyte)sh1;     //                            00101100    44
            Console.WriteLine(sb1);

            // checked => checked permet d’activer explicitement le contrôle de dépassement de capacité 
            // pour les conversions et les opérations arithmétiques de type intégral
            //checked
            //{
            //    sb1 = (sbyte)sh1;
            //    Console.WriteLine(sb1);   // avec checked une exception est générée, s'il y a un dépassement de capacité
            //}

            // Par défaut la vérification des dépassement de capacités est unchecked
            // si l'on veut qu'elles soient par défaut checked: il faut choisir l'option checked dans les propriétés du projet dans Advanced Build Settings
            // Dans ce cas, on utilise unchecked pour désactiver la vérification des dépassements
            unchecked
            {
                sb1 = (sbyte)sh1;
                Console.WriteLine(sb1);     // plus de vérification de dépassement de capacité
            }
            #endregion

            #region Fonction de conversion
            // La classe Convert contient des méthodes statiques permettant de convertir un entier vers un double, ...
            // On peut aussi convertir une chaine de caractères en un type numérique
            int fc1 = 42;
            double cnv = Convert.ToDouble(fc1);    // Convertion d'un entier en double
            Console.WriteLine(cnv);

            int cnv1 = Convert.ToInt32("123");
            Console.WriteLine((cnv1 + 1) + " " + "123" + "1");

            // int cnv2 = Convert.ToInt32("azerty"); //  Erreur => génère une exception formatException
            // Conversion d'un nombre sous forme de chaine dans une autre base
            Console.WriteLine(Convert.ToString(cnv1, 2)); // binaire
            Console.WriteLine(Convert.ToString(cnv1, 16)); // hexa

            // Conversion d'une chaine de caractères en entier
            // Parse
            int cnv3 = int.Parse("345");
            //cnv3 = int.Parse("azery");     //  Erreur => génère une exception formatException
            Console.WriteLine(cnv3);
            double cnv4 = double.Parse("3,14");
            Console.WriteLine(cnv4);

            // TryParse
            int cnv5;
            bool tstCnv = int.TryParse("123", out cnv5); // Retourne true et la convertion est affecté à cnv5
            Console.WriteLine(tstCnv + " " + cnv5);
            tstCnv = int.TryParse("123aaa", out cnv5);  // Erreur => retourne false, 0 est affecté à cnv4
            Console.WriteLine(tstCnv + " " + cnv5);
            #endregion

            #region type référence
            StringBuilder s1 = new StringBuilder("hello");
            Console.WriteLine(s1);
            StringBuilder s2 = null;
            Console.WriteLine(s1 + " " + s2);

            s2 = s1;
            Console.WriteLine(s1 + " " + s2);

            s1 = null;
            Console.WriteLine(s1 + " " + s2);

            s2 = null;  // str1 et str2 sont égales à null
                        // Il n'y a plus de référence sur l'objet, il éligible à la destruction par le garbage collector
            Console.WriteLine(s1 + " " + s2);
            #endregion

            #region type nullable
            // Nullable : permet d'utiliser une valeur null avec un type valeur
            double? tn1 = null;

            Console.WriteLine(tn1.HasValue); // tn1 == null retourne false
            tn1 = 3.67;                      // Conversion implicite(double vers nullable)
            Console.WriteLine(tn1.HasValue); // La propriété HasValue retourne true si  tn contient une valeur (!= null)

            Console.WriteLine(tn1.Value);   // Pour récupérer la valeur, on peut utiliser la propriété Value
            Console.WriteLine((double)tn1); // ou, on peut faire un cast
            #endregion

            #region Constante
            const double PI = 3.14;
            Console.WriteLine(PI * 2);
            //PI = 3.1419;  // Erreur: on ne peut pas modifier la valeur d'une constante
            #endregion

            #region Format de chaine de caractère
            int xi = 1;
            int yi = 3;

            string resFormat = string.Format("xi={0} yi={1}", xi, yi);
            Console.WriteLine(resFormat);
            Console.WriteLine("xi={0} yi={1}", xi, yi); // on peut définir directement le format dans la mèthode WriteLine

            Console.WriteLine($"xi={xi} yi={yi}");

            Console.WriteLine("c:\tmp\newfile.txt"); // \t et \n sont considérés comme des caractères spéciaux

            // Caractères spéciaux
            // \n       Nouvelle ligne
            // \r       Retour chariot
            // \f       Saut de page
            // \t       Tabulation horizontale
            // \v       Tabulation verticale
            // \0       Caractère nul
            // \a 	    Alerte
            // \b       Retour arrière
            // \\       Backslash
            // \'       Apostrophe
            // \"       Guillemet

            // Chaînes textuelles => @ devant une littérale chaine de caractères
            // Conserve les retours à la ligne, n'interpréte pas les caractères spéciaux (utile pour les chemins de fichiers)
            Console.WriteLine(@"c:\tmp\newfile.txt");
            #endregion

            #region Saisie clavier
            Console.Write("Saisie Clavier :");
            string line = Console.ReadLine();
            Console.WriteLine(line);
            #endregion

            #region Exercice Salutation
            //Faire un programme qui:
            //- Affiche le message: Entrer votre nom
            //- Permet de saisir le nom
            //- Affiche Bonjour, complété du nom saisie

            Console.Write("Entrer votre nom=");
            string nom = Console.ReadLine();
            Console.WriteLine($"Bonjour, {nom}");
            #endregion

            #region Operateur
            // opérateur arithmétique
            int op1 = 12;
            int op2 = 3;
            int opRes = op1 + op2;
            int opRes2 = 11 % 2;    // % => modulo (reste de division entière)
            Console.WriteLine($" + {opRes}   %{opRes2}");

            //Division par 0

            // Entier
            // int zero = 0;
            // Console.WriteLine(5/zero); // => exception DivideByZeroException

            // Nombre à virgule flottante
            Console.WriteLine(1.0 / 0.0); // => Infinity
            Console.WriteLine(-1.0 / 0.0); // => -Infinity
            Console.WriteLine(0.0 / 0.0); // => NaN

            // Exercice Somme
            // Saisir 2 chiffres et afficher le résultat dans la console sous la forme 1 + 3 = 4
            Console.Write("Saisr un entier: ");
            int va = Convert.ToInt32(Console.ReadLine());
            Console.Write("Saisr un entier: ");
            int vb = Convert.ToInt32(Console.ReadLine());
            int add = va + vb;
            Console.WriteLine($"{va} + {vb} = {add}");
            // Console.WriteLine($"{va} + {vb} = {va+vb}");

            // Incrémentation
            // pré-incrémentation
            int inc = 0;
            int res = ++inc;
            Console.WriteLine($"{inc} {res}"); //inc=1 res=1

            // post-incrémentation
            inc = 0;
            res = inc++;
            Console.WriteLine($"{inc} {res}"); // res=0 inc=1

            // Affectation composée
            inc = 23;
            inc += 2; // inc=inc+2
            Console.WriteLine(inc);

            // Opérateur de comparaison
            bool cmp1 = inc > 100;   // Une comparaison a pour résultat un booléen
            bool cmp2 = inc == 25;   // true
            Console.WriteLine($"cmp1= {cmp1} cmp2= {cmp2}");

            // Opérateur logique

            // Opérateur Non !
            Console.WriteLine(!(inc == 25)); // !(true) => false

            bool cmp3 = inc > 0 && inc < 100;
            Console.WriteLine(cmp3);

            // Opérateur court-circuit && et ||
            // && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
            bool cmpCcEt = inc != 25 && inc < 100; // comme inc != 25 est faux, inc < 100 n'est pas évalué
            Console.WriteLine(cmpCcEt);

            // || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
            bool cmpCcOu = inc == 25 || inc > 100;  // comme inc == 25 est vraie, inc > 100 n'est pas évalué
            Console.WriteLine(cmpCcOu);

            cmpCcEt = inc != 25 && inc++ < 100; // inc++ < 100 n'est pas exécuté, inc n'est pas incrémenté
            Console.WriteLine($"{cmpCcEt} inc={inc}");

            string str = null;
            bool cmp5 = str != null && str.ToUpper() == "AZERTY";

            // Opérateur Binaires (bit à bit)
            byte b = 0b11010;
            Console.WriteLine(Convert.ToString(~b, 2));         // Opérateur ~ => complémént: 1-> 0 et 0-> 1
            Console.WriteLine(Convert.ToString(b & 0b110, 2));  //          et => 10
            Console.WriteLine(Convert.ToString(b | 0b110, 2));  //          ou => 11110
            Console.WriteLine(Convert.ToString(b ^ 0b110, 2));  // ou exclusif => 11100
            Console.WriteLine(Convert.ToString(b << 2, 2));     // Décalage à gauche de 2 (insertion de 2 0 à droite) => 1101101000 
            Console.WriteLine(Convert.ToString(b >> 3, 2));     // Décalage à droite de 3 (insertion de 3 0 à gauche) => 110

            // L’opérateur de fusion null ?? retourne la valeur de l’opérande de gauche si elle n’est pas null
            // sinon, il évalue l’opérande de droite et retourne son résultat
            string str5 = "azerty";
            string strRes1 = str5 ?? "Default";
            Console.WriteLine(strRes1);     // azerty
            str5 = null;
            strRes1 = str5 ?? "Default";    // Default
            Console.WriteLine(strRes1);
            #endregion

            #region Promotion numérique

            // prn1 est promu en double => Le type le + petit est promu vers le +grand type des deux
            int prn1 = 11;
            double prn2 = 123.45;
            double prnRes = prn1 + prn2;
            Console.WriteLine(prnRes);
            Console.WriteLine(prn1 / 2);    // 5
            Console.WriteLine(prn1 / 2.0);  // 5.5   prn1 est promu en double

            sbyte b1 = 1;
            sbyte b2 = 2;       // sbyte, byte, short, ushort, char sont promus en int
            int b3 = b1 + b2;     // b1 et b2 sont promus en int
            Console.WriteLine(b3);
            #endregion

            // Exercice Moyenne: 
            // Saisir 2 nombres entiers et afficher la moyenne dans la console
            Console.Write("Saisir un entier: ");
            int m1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Saisir un entier: ");
            int m2 = Convert.ToInt32(Console.ReadLine());
            double mr = (m1 + m2) / 2.0;
            Console.WriteLine($"La moyenne= {mr}");

            #region enumération
            // dir est une variable qui ne pourra accepter que les valeurs de l'enum Direction
            Direction dir = Direction.NORD;

            // enum -> string
            // La méthode toString convertit une constante enummérée en une chaine de caractère
            string dirStr = dir.ToString();
            Console.WriteLine(dirStr);

            // enum ->  entier (cast)
            int iDir = (int)dir;
            Console.WriteLine(iDir);

            // string -> enum
            // La méthode Parse permet de convertir une chaine de caractère en une constante enummérée
            Motorisation m = (Motorisation)Enum.Parse(typeof(Motorisation), "GPL");
            Console.WriteLine(m);

            // m = (Motorisation)Enum.Parse(typeof(Motorisation), "GPL2"); // Si la chaine n'existe pas dans l'énumation => exception
            //Console.WriteLine(m);

            // entier -> enum
            iDir = 270; //271
            if (Enum.IsDefined(typeof(Direction), iDir))    // Permet de tester si la valeur entière existe dans l'enumération
            {
                dir = (Direction)iDir;
                Console.WriteLine(dir.ToString());
            }

            // Énumération comme indicateurs binaires
            Jours jourSemaine = Jours.LUNDI | Jours.MARDI;
            if ((jourSemaine & Jours.LUNDI) != 0)    // teste la présence de LUNDI
            {
                Console.WriteLine("Lundi");
            }

            if ((jourSemaine & Jours.MERCREDI) != 0)    // teste la présence de Mercredi
            {
                Console.WriteLine("Mercredi");
            }
            // jourSemaine = jourSemaine | Jours.SAMEDI;
            jourSemaine |= Jours.SAMEDI;
            if ((jourSemaine & Jours.WEEKEND) != 0)
            {
                Console.WriteLine("WeekEnd");
            }
            Console.WriteLine(jourSemaine.ToString());

            switch (dir)
            {
                case Direction.NORD:
                    Console.WriteLine("Nord");
                    break;
                case Direction.OUEST:
                    Console.WriteLine("Ouest");
                    break;
                default:
                    Console.WriteLine("Autre direction");
                    break;
            }


            #endregion
            Point pt1;
            pt1.X = 3;  // accès au champs X de la structure
            pt1.Y = -1;
            Console.WriteLine($" P1 (X={pt1.X}, Y={pt1.Y})");

            // Affectation d'une structure
            Point pt2 = pt1; // Les champs de pt2 sont initialisé avec les valeurs des champs de pt1
            Console.WriteLine($" P2 (X={pt2.X}, Y={pt2.Y})");
            pt1.X = 5;
            Console.WriteLine($" P1 (X={pt1.X}, Y={pt1.Y})");
            Console.WriteLine($" P2 (X={pt2.X}, Y={pt2.Y})");

            // Comparaison de 2 structures => Il faut comparer chaque champs
            if (pt1.X == pt2.X && pt1.Y == pt2.Y)
            {
                Console.WriteLine("Mes points sont égaux");
            }
            Console.ReadKey();
        }
    }
}

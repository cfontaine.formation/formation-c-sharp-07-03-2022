﻿
using _07_EspaceDeNoms.dao;
using _07_EspaceDeNoms.gui;
using ConsoleGui = _07_EspaceDeNoms.gui.Console;

// ALias
using ConsoleSys = System.Console;


namespace _07_EspaceDeNoms
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // _07_EspaceDeNoms.gui.Window win = new _09_EspaceDeNoms.gui.Window();
            Window win = new Window();
            //  System.Console.WriteLine();
            //  _07_EspaceDeNoms.gui.Console cnx = new _09_EspaceDeNoms.gui.Console();

            // avec un Alias
            ConsoleGui cnx = new ConsoleGui();
            ConsoleSys.WriteLine();

            UserDao uDao = new UserDao();
        }
    }
}


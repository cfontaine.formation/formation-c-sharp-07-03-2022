﻿using _14A_BibliothequeBdd;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

namespace _14B_Ado_Net
{
    class Program
    {
        // Pour PostGreSQL, le fournisseur de donnée Npgsql est à obtenir avec nuget
        // - NpgsqlConnection -> établir une connexion à une bdd
        // - NpgsqlCommand -> exécuter une ou plusieurs requêtes Sql
        // - NpgsqlDataReader -> accès séquentiel au jeu de données
        // - NpgsqlDataParameter représente un paramètre de commande
        // Exemple de chaine de connection pour PostGreSQL: Host=localhost;Username=postgres;Password=secret;Database=testdb"

        // Pour ajouter au projet la bibliothèque
        // Cliquer droit sur le projet -> Ajouter -> référence -> choisir 14A-Bibliotheque_bdd_dao
        static void Main(string[] args)
        {
            Contact c = new Contact("John", "Doe", DateTime.Now, "jd@dawan.com");
            // Test de connexion à la base de donnée (SQLServer)
            TestBdd(c);

            // Test de Contact dao
            TestDao();
            Console.ReadKey();
        }

        static void TestBdd(Contact c)
        {
            // La chaine de connection contient toutes les informations pour la connection à la base de donnée
            // Server-> adresse du serveur de bdd
            // Port port de serveur de bdd
            // Database -> nom de la base de donnée
            // Uid -> utilisateur de la base de donnée
            // Pwd -> mot de passe de la bdd

            string chaineConnection = @"Data Source=DESKTOP-9H6VFME\SQLEXPRESS;Initial Catalog=formationmars;Integrated Security=True";
            // Création de la connexion à la base de donnée SqlConnection
            SqlConnection cnx = new SqlConnection(chaineConnection);
            // Ouverture de la connexion à la base de donnée
            cnx.Open();
            string req = "INSERT INTO contacts(prenom,nom,date_naissance,email) VALUES(@prenom,@nom,@dateNaissance,@email);";
            // Création de l'objet SqlCommand qui permet d'executer la requête SQL
            SqlCommand cmd = new SqlCommand(req, cnx);
            // Remplacement des paramètres dans la requête (@...) par les valeurs
            cmd.Parameters.AddWithValue("@prenom", c.Prenom);
            cmd.Parameters.AddWithValue("@nom", c.Nom);
            cmd.Parameters.AddWithValue("@dateNaissance", c.DateNaissance);
            cmd.Parameters.AddWithValue("@email", c.Email);
            cmd.ExecuteNonQuery();  // execution de la requete pour INSERT,UPDATE,DELETE
            req = "SELECT prenom,nom,date_naissance,email FROM contacts";
            cmd = new SqlCommand(req, cnx);
            SqlDataReader read = cmd.ExecuteReader();   // execution de la requete pour SELECT, on récupère le résultat de la requête dans un objet SqlDataReader
            while (read.Read()) // Read() permet de passer à la prochaine "ligne" retourne false quand il n'y a plus de resultat
            {
                // GetXXXX(string NumeroColonne) pour récupérer les valeurs
                Console.WriteLine($"{read.GetString(0)} {read.GetString(1)} {read.GetDateTime(2)} {read.GetString(3)}");
            }
            cnx.Close();// Fermeture de la connection
            cnx.Dispose();
        }

        static void TestDao()
        {
            ContactDao dao = new ContactDao();
            //Récupération de la chaine de connection dans le fichier App.config du projet élément<connectionStrings>
            dao.ChaineConnection = ConfigurationManager.ConnectionStrings["chCnxSqlSever"].ConnectionString;
            Contact c = SaisirContact();
            Console.WriteLine("id={0}", c.Id); // id=0 => l'objet n'a pas été peristé dans la base de donnée
            dao.SaveOrUpdate(c); // persister l'objet dans la bdd
            Console.WriteLine("id={0}", c.Id); // id a été généré par la bdd
            Console.WriteLine("{0}", c);
            long id = c.Id;
            // Affichage de tous les objets contact
            List<Contact> lst = dao.FindAll();
            foreach (Contact co in lst)
            {
                Console.WriteLine(co);
            }

            // Lire un objet à partir de son id
            Console.WriteLine("\nLire {0}", id);
            Contact cr = dao.FindById(id);
            Console.WriteLine(cr);

            // Modification 
            Console.WriteLine("\nModification {0}", cr.Id);
            cr.Prenom = "Marcel";
            cr.DateNaissance = new DateTime(1987, 8, 11);
            dao.SaveOrUpdate(cr);
            cr = dao.FindById(cr.Id);
            Console.WriteLine(cr);

            // Effacer
            Console.WriteLine("\neffacer {0}", cr.Id);
            dao.Delete(cr);
            lst = dao.FindAll();
            foreach (Contact co in lst)
            {
                Console.WriteLine(co);
            }
        }

        private static Contact SaisirContact()
        {
            Console.Write("Entrer votre prénom: ");
            string prenom = Console.ReadLine();
            Console.Write("Entrer votre nom: ");
            string nom = Console.ReadLine();
            Console.Write("Entrer votre date de naissance (YYYY/MM/DD): ");
            DateTime jdn = DateTime.Parse(Console.ReadLine());
            Console.Write("Entrer votre email: ");
            string email = Console.ReadLine();
            return new Contact(prenom, nom, jdn, email);
        }

    }

}


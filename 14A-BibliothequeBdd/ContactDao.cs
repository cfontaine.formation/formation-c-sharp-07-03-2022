﻿
using System.Collections.Generic;
using System.Data.SqlClient;

namespace _14A_BibliothequeBdd
{
    public class ContactDao
    {
        public string ChaineConnection { get; set; }
        public void SaveOrUpdate(Contact c)
        {
            if (c.Id == 0)
            {
                Create(c);
            }
            else
            {
                Update(c);
            }
        }

        public void Delete(Contact contact)
        {
            using (SqlConnection cnx = new SqlConnection(ChaineConnection)) // using -> fermeture automatique de la caonnexion à la bdd
            {
                cnx.Open();
                string req = "DELETE FROM contacts WHERE id=@id";
                SqlCommand cmd = new SqlCommand(req, cnx);
                cmd.Parameters.AddWithValue("@id", contact.Id);
                cmd.ExecuteNonQuery();
            }

        }

        public Contact FindById(long id)
        {
            Contact c = null;
            using (SqlConnection cnx = new SqlConnection(ChaineConnection))
            {
                cnx.Open();
                string req = "SELECT prenom,nom,date_naissance,email,id FROM contacts WHERE id=@id";
                SqlCommand cmd = new SqlCommand(req, cnx);
                cmd.Parameters.AddWithValue("id", id);
                SqlDataReader read = cmd.ExecuteReader();
                if (read.Read())
                {
                    c = new Contact(read.GetString(0), read.GetString(1), read.GetDateTime(2), read.GetString(3));
                    c.Id = id;
                }
            }
            return c;
        }

        public List<Contact> FindAll()
        {
            List<Contact> lst = new List<Contact>();
            using (SqlConnection cnx = new SqlConnection(ChaineConnection)) // using -> fermeture automatique de la caonnexion à la bdd
            {
                cnx.Open();
                string req = "SELECT id, prenom,nom,date_naissance,email,id FROM contacts";
                SqlCommand cmd = new SqlCommand(req, cnx);
                SqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    Contact tmp = new Contact(read.GetString(1), read.GetString(2), read.GetDateTime(3), read.GetString(4));
                    tmp.Id = read.GetInt64(0);
                    lst.Add(tmp);
                }
            }
            return lst;
        }

        // On peut ajouter d'autre méthodes dans le Dao findByEmail, ... voir ContactDaoGen

        private void Create(Contact contact)
        { 
            using (SqlConnection cnx = new SqlConnection(ChaineConnection))
            {
                cnx.Open();
                string req = "INSERT INTO contacts(prenom,nom,date_naissance,email) VALUES(@prenom,@nom,@dateNaissance,@email);";
                SqlCommand cmd = new SqlCommand(req, cnx);
                cmd.Parameters.AddWithValue("@prenom", contact.Prenom);
                cmd.Parameters.AddWithValue("@nom", contact.Nom);
                cmd.Parameters.AddWithValue("@dateNaissance", contact.DateNaissance);
                cmd.Parameters.AddWithValue("@email", contact.Email);
                cmd.ExecuteNonQuery();
                //c.Id = (long)cmd?.ExecuteScalar();
            }
        }

        private void Update(Contact contact)
        {
            using (SqlConnection cnx = new SqlConnection(ChaineConnection))
            {
                cnx.Open();
                string req = "UPDATE contacts SET nom = @nom, prenom=@prenom, date_naissance=@dateNaissance, email=@email  WHERE id=@id";
                SqlCommand cmd = new SqlCommand(req, cnx);
                cmd.Parameters.AddWithValue("@id", contact.Id);
                cmd.Parameters.AddWithValue("@prenom", contact.Prenom);
                cmd.Parameters.AddWithValue("@nom", contact.Nom);
                cmd.Parameters.AddWithValue("@dateNaissance", contact.DateNaissance);
                cmd.Parameters.AddWithValue("@email", contact.Email);
                cmd.ExecuteNonQuery();
            }
        }
    }
}


﻿using System;

namespace _14A_BibliothequeBdd
{
    public class Contact // si la classe est public -> elle est visible à l'exterieur de la bibliothèque
    {                    // sinon une classe est par défaut internal -> visible uniquement à l'interieur de la bibliothèque
        public long Id { get; set; }
        public string Prenom { get; set; }
        public string Nom { get; set; }
        public DateTime DateNaissance { get; set; }
        public string Email { get; set; }

        public Contact(string prenom, string nom, DateTime dateNaissance, string email)
        {
            Prenom = prenom;
            Nom = nom;
            DateNaissance = dateNaissance;
            Email = email;
        }

        public override string ToString()
        {
            return $"{Id} {Prenom} {Nom} {DateNaissance} {Email}";
        }

        public override bool Equals(object obj)
        {
            return obj is Contact contact &&
                   Id == contact.Id;
        }

        public override int GetHashCode()
        {
            return 2108858624 + Id.GetHashCode();
        }
    }
}
